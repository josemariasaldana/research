[Home](/README.md)

# Resumen del cv
**Actualizado abril 2023**

Estudié Ingeniería de Telecomunicación en la Universidad de Zaragoza (Cursos 92/93 a 96/97), presentando el Proyecto en enero de 1998. En 2006 decidí retomar mi formación en el ámbito de las Telecomunicaciones, y me matriculé en el “Máster de Tecnologías de la Información y Comunicaciones en Redes Móviles”. Al terminar el Máster en 2008, me incorporé al Instituto de Investigación en Ingeniería de Aragón (I3A), trabajando desde entonces como investigador contratado para diferentes proyectos en el Área de Telemática. Presenté la Tesis Doctoral el 2 de diciembre de 2011. Posteriormente estuve contratado como “Doctor Senior”, con contrato de Acceso al Sistema Español de Ciencia, Tecnología e Innovación. En 2020 pasé a CIRCE Centro Tecnológico, donde trabajo como Responsable de Proyecto, en relación con comunicaciones industriales y en instalaciones energéticas.

## Proyectos

[Más info](https://gitlab.com/josemariasaldana/research/-/blob/main/projects.md#rd-projects-funded-through-competitive-calls-of-public-or-private-entities).

Investigador Principal de un proyecto financiado por Orange Labs (Francia): [Wireless LAN based on use of Light Virtual WiFi Access Points](https://i3a.unizar.es/en/projects/wireless-lan-based-use-light-virtual-wifi-access-points), Orange Labs Collaborative Research Contract J02181. 1/6/2019-31/5/2020.

He sido líder del WP3 del Proyecto [H2020 Wi-5, What to do With the Wi-Fi Wild West](https://github.com/Wi5) ([Grant agreement no: 644262](https://cordis.europa.eu/project/id/644262), enero 2015-abril 2018).

He participado en otro proyecto europeo (Celtic) y en otros  proyectos de investigación nacionales y autonómicos, y también en convenios y contratos con empresas.   

## Producción científica  

Mis  líneas de investigación principales están relacionadas con las redes programables, cableadas e inalámbricas, y en el estudio de servicios con requisitos tiempo real, en entornos industriales y también multimedia, como voz sobre IP o videojuegos online.

He publicado **23 artículos en revistas JCR** (13 de ellos como primer autor), y 5 artículos en otras revistas internacionales. 

He participado en la publicación de **42 artículos en congresos internacionales**, y **20 nacionales**.  

## Estandarización en el IETF (Internet Engineering Task Force)

Fui el coordinador del documento RFC 7962 “Alternative Network Deployments: Taxonomy, Characterization, Technologies and Architectures,” en el GAIA (Global Access to the Internet for All) group.  

## Ediciones y Revisiones

Desde el 9/3/2017 soy miembro del Consejo Editorial de la revista IEEE Access, ISSN: 2169-3536 Impact Factor 2022: 3.9. 

Desde 21/9/2017 soy Area Editor y miembro del Consejo Editorial de la revista KSII Transactions on Internet and Information Systems, Impact Factor 2022: 1.5.

He realizado  revisiones de 30 artículos para revistas JCR, y he sido invitado al  comité científico de 36 congresos internacionales, participando en el  comité de organización de 10 de ellos.   

## Docencia  y supervisión de estudiantes 
Director de la tesis “IoTsafe: Método y sistema de comunicaciones seguras para ecosistemas IoT” (Autor Jorge David de Hoz Diego). 

He dirigido 6 Proyectos Fin de Carrera, 5 Trabajos Fin de Grado y un Trabajo Fin de Máster.  

## Sesiones invitadas y tutoriales  
Sesión invitada en la Universidad de Zagreb, invitado por la Sección Croata del IEEE Communications Society, 2012. 
Sesión invitada en el Workshop NIME del  congreso IEEE CCNC, enero 2014, Las Vegas. 
Sesión invitada en la Liverpool John Moores University en 2015.   

## Estancias  
Estancia de tres meses en la Universidad de Bolonia may-jul 2012, para la que obtuve una beca de ámbito autonómico.  
Estancia de tres meses en TNO (Holanda) ene-mar 2017. 

## Habilitación como Profesor Contratado Doctor  
Con fecha 14/3/2014 recibí la evaluación positiva de la ANECA como  Profesor Contratado Doctor y Profesor de Universidad Privada. 
