[Home](/README.md)

# Summary of the CV
**Updated Dec 2022**

I studied Telecommunications Engineering at University of Zaragoza (1992-1998). After some years, I came back to Academia and studied a Master in ICT in Mobile Networks (2008). I finished my PhD in 2011. 

## Projects

[More info](https://gitlab.com/josemariasaldana/research/-/blob/main/projects.md#rd-projects-funded-through-competitive-calls-of-public-or-private-entities).

I was the Principal Investigator of a Research Contract financed by Orange Labs, France (Wireless LAN based on use of Light Virtual WiFi Access Points, Orange Labs Collaborative Research Contract J02181).

I have been the leader of the WP3 of the H2020 Wi-5, What to do With the Wi-Fi Wild West (Grant agreement no: 644262, Jan 2015-Apr 2018). 

I have participated in other European Projects (H2020, Horizon Europe, Celtic) and other national and regional ones, and also in contracts and projects with companies. 


## Scientific activities 

My main research lines are related to programmable networks (both wired and wireless), real-time services as VoIP and online games. During these years I have published **23 articles in Journals indexed in Clarivate List of Journals** (13 of them as first author), and 5 articles in other international journals. I have also published **42 articles in international conferences**, and **20 in Spanish national conferences**. 


## Standardisation

I published an IETF document: RFC 7962 “Alternative Network Deployments: Taxonomy, Characterization, Technologies and Architectures,” en el GAIA (Global Access to the Internet for All) group. 


## Editorships and reviews

Since 9/3/2017 I am a member of the Editorial Board of IEEE Access, ISSN: 2169-3536 Impact Factor 2022: 3.9.

Since 21/9/2017 I am an Area Editor and member of the Editorial Board of KSII Transactions on Internet and Information Systems, Impact Factor 2022: 1.5. 

I have reviewed 30 articles for 30 JCR journals, and I have participated in the TPC of 36 international conferences, and in the Organisation Committee of 10 or these conferences. 


## Teaching and supervision of students 

I supervised a PhD thesis titled “[IoTsafe: Método y sistema de comunicaciones seguras para ecosistemas IoT](https://zaguan.unizar.es/record/94492?ln=es)” (Author Jorge David de Hoz Diego).

I have supervised 6 Final Degree Projects, 5 Final Degree Works and 1 Masters’ Degree Project (equivalent to 156 hours). See [details here](supervision.md#final-year-projects).

## Invited talks 

“Online games: a real-time problem for the network,” Department of Computer Science, Liverpool John Moores University, UK, Oct. 2015. 
NIME Workshop

Invited Talk: “The problem of using a best-effort network for online games,” IEEE CCNC 2014, Jan., Las Vegas, NV, USA. 

Tutorial “Online Games: Traffic Characterization and Network Support,” IEEE Consumer Communications and Networking Conference, CCNC 2014, Las Vegas, NV, USA.

“Online games tutorial,” Transport Area Open Meeting, Aug 2013, IETF87, Berlin, Germany. 

“Online games: is the Internet prepared for them?” IEEE Communications Society, Croatian Section. FER (Fakultet Elektrotehnikei Računarstva), University of Zagreb, Croatia, May 2012. 


## Fellowships 

Jan – Apr 2017: Research Scholarship, TNO, The Hague, The Netherlands. 

May – July 2012: Research Scholarship, Department Scienze dell'informazione, Università di Bologna, Italy.


## Evaluation as “Profesor Contratado Doctor” 

ANECA positive evaluation as  Profesor Contratado Doctor (14/3/2014)
