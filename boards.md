[Home](README.md)

# Editorial Boards and reviews

## Member of the Editorial Board of two journals indexed in JCR

[IEEE Access](http://ieeeaccess.ieee.org/) (Associate Editor).

[KSII Transactions on Internet and Information Systems](http://www.itiis.org/) (Area Editor)

## Reviews in other journals

Reviewer of [MDPI Sensors](https://www.mdpi.com/journal/sensors), Mar 2025.

Reviewer of [IEEE Power Engineering Letters](https://ieee-pes.org/publications/pes-letters/), Apr 2024.

Reviewer of [IEEE Access](http://ieeeaccess.ieee.org/), Jan 2017, Feb 2017, Jun 2017, Jan 2020, Jun 2023, Oct 2023, Jan 2024.

Reviewer of [IEEE Power Engineering Letters](https://ieee-pes.org/publications/pes-letters/), Jan 2024.

Reviewer of [IEEE Transactions on Network and Service Management](https://www.comsoc.org/publications/journals/ieee-tnsm), 2020, Jun 2023.

Reviewer of [Wireless Communications and Mobile Computing](https://www.hindawi.com/journals/wcmc/), Feb 2020.

Reviewer of [IEEE Letters of the Computer Society](https://www.computer.org/csdl/journal/lc), Aug 2019.

Reviewer of [Computer Communications](https://www.journals.elsevier.com/computer-communications) (Elsevier), Apr 2019.

Reviewer of [IEEE Wireless Communications](https://www.comsoc.org/publications/magazines/ieee-wireless-communications) magazine, Nov 2016, Feb 2019.

Reviewer of [IEEE Transactions on Communications](https://www.comsoc.org/publications/journals/ieee-tcom), Nov 2018.

Reviewer of [IEEE Communications Letters](http://www.comsoc.org/cl), 2012, 2015, Jul 2017, May 2018, Nov 2018.

Reviewer of [IEEE Journal on Selected Areas in Communications](http://www.comsoc.org/jsac): Special Issue on Adaptive Media Streaming, 2013. Sep 2017 (Special Issue on [Advances in Satellite Communications](http://www.comsoc.org/jsac/cfp/advances-satellite-communications)). Oct 2018 (Series on [Network Softwarization and Enablers](https://www.comsoc.org/jsac/series/netsoft-enablers)).

Reviewer of [IEEE Network](http://www.comsoc.org/netmag), Sep 2013, 2014, 2015, Mar 2017, May 2018.

Reviewer of [Wireless Networks, The Journal of Mobile Communication, Computation and Information](https://link.springer.com/journal/11276) (Springer), Jul 2017, Jan 2018.

Reviewer of [IEEE Communications Magazine](http://www.comsoc.org/commag), 2013, Sep 2016, Jan 2018, Jun 2019, Nov 2023.

Reviewer of [International Journal of Communication Systems](http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-1131/), Jul 2017.

Reviewer of IET, [The Journal of Engineering](http://digital-library.theiet.org/content/journals/joe/), Jul 2016.

Reviewer of [Computer Networks](http://www.journals.elsevier.com/computer-networks/) (Elsevier), 2012, Oct 2016.

Reviewer of [Computers in Human Behaviour](http://www.journals.elsevier.com/computers-in-human-behavior/), 2014.

Reviewer of [International Journal of Ad Hoc and Ubiquitous Computing](http://www.inderscience.com/jhome.php?jcode=ijahuc), 2014. 

Reviewer of [Journal of Visual Communication and Image Representation](http://www.journals.elsevier.com/journal-of-visual-communication-and-image-representation/), Elsevier, 2013. 

Reviewer of [KSII Transactions on Internet and Information Systems](http://www.itiis.org/), 2012.

Reviewer of [Multimedia Systems](http://www.springer.com/computer/information+systems+and+applications/journal/530), Springer, Special Issue "Network and Systems Support for Games," 2012.

## Topic Editor

Topic editor of [Frontiers in Communications and Networks](https://www.frontiersin.org/journals/communications-and-networks), [Intelligent Approaches for Performance Optimizations and Energy Efficiency in B5G/6G Mobile (Communication) Networks](https://www.frontiersin.org/research-topics/61467/intelligent-approaches-for-performance-optimizations-and-energy-efficiency-in-b5g6g-mobile-communication-networks). JIF 2023: 2.1. 
CATEGORY TELECOMMUNICATIONS, 71/119, JIF QUARTILE Q3, JIF PERCENTILE 40.8, Emerging Sources Citation Index (ESCI). (See [this](https://jcr.clarivate.com/jcr-jp/journal-profile?journal=FRONT%20COMMUN%20NETW&year=2023))