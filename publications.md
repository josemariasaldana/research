[Home](/README.md)

## Publications

[2025](#anchor-2025) -
[2024](#anchor-2024) -
[2023](#anchor-2023) -
[2022](#anchor-2022) -
[2021](#anchor-2021) -
[2020](#anchor-2020) -
[2019](#anchor-2019) -
[2018](#anchor-2018) -
[2017](#anchor-2017) -
[2016](#anchor-2016) -
[2015](#anchor-2015) -
[2014](#anchor-2014) -
[2013](#anchor-2013) -
[2012](#anchor-2012) -
[2011](#anchor-2011) -
[2010](#anchor-2010) -
[2009](#anchor-2009)

### 2025

#### International Journals

Konstantinos F. Krommydas, Christos-Spyridon G. Karavas, Konstantinos A. Plakas, Efthimia Chassioti, Ioannis Moraitis, Anibal Antonio Prada Hurtado, **Jose Saldana**, Eduardo Martinez Carrasco, Virgilio De Andrade, Dalibor Brnobic. "[_Enhancing the Operation of the Hellenic Transmission System through Wide Area Monitoring and Control_](https://ieeexplore.ieee.org/document/10820144)", in [IEEE Power and Energy Magazine](https://ieee-pes.org/publication-item/power-energy-magazine/), vol. 23, no. 1, pp. 99-112, Jan.-Feb. 2025. doi: [10.1109/MPE.2024.3435171](https://doi.org/10.1109/MPE.2024.3435171) (Journal Impact Factor 2023: 3.1, ENGINEERING, ELECTRICAL & ELECTRONIC, 134/352, Q2. **2025 Journal Impact Factor pending**). JCR #25.

#### International Conferences

Aníbal Antonio Prada Hurtado, Mario Manana, Maria Teresa Villén Martínez, Eduardo Martinez Carrasco, **Jose Saldana**, Konstantinos F. Krommydas, Konstantinos A. Plakas, Christos-Spyridon G. Karavas, Efthimia Chassioti, Ioannis Moraitis, "_Implementation of a Novel Wide-Area Short-Circuit Protection Method on the Greek Transmission System_," 2025 [IEEE PES General Meeting](https://pes-gm.org/), 27–31 July 2025 in Austin, Texas, **Accepted for presentation**.

### 2024

#### International Journals

Esteban Damián Gutiérrez Mlot, **Jose Saldana**, Ricardo J. Rodríguez, Igor Kotsiuba, Carlos H. Gañan, "[_A Dataset to Train Intrusion Detection Systems based on Machine Learning Models for Electrical Substations,_](https://www.sciencedirect.com/science/article/pii/S2352340924011156)" [Data in Brief](https://www.sciencedirect.com/journal/data-in-brief), Volume 57, 2024, 111153, ISSN 2352-3409, doi: [10.1016/j.dib.2024.111153](https://doi.org/10.1016/j.dib.2024.111153) (Journal Impact Factor 2023: 1.0, Multidisciplinary Sciences 79/134, Q3. **2024 Journal Impact Factor pending**). JCR #24.

Victor Malumbres, **Jose Saldana**, Gonzalo Berne, Julio Modrego, "[_Firmware Updates Over The Air via LoRa: Unicast and Broadcast Combination for Boosting Update Speed_](https://www.mdpi.com/1424-8220/24/7/2104)," in [Sensors](https://www.mdpi.com/journal/sensors)  2024, 24(7), 2104. doi: [10.3390/s24072104](https://doi.org/10.3390/s24072104), [**Open Access**](https://www.mdpi.com/1424-8220/24/7/2104/pdf). (Journal Impact Factor 2023: 3.4, CHEMISTRY, ANALYTICAL Q2, 34/106; ENGINEERING, ELECTRICAL & ELECTRONIC 122/352, Q2; INSTRUMENTS & INSTRUMENTATION 24/76, Q2. **2024 Journal Impact Factor pending**). JCR #23.

**Jose Saldana**, Miguel A Olivan, "[_Lossless and Stateless Compression of IEC 61850 Sampled Values Flows_](https://ieeexplore.ieee.org/document/10415521)," in [IEEE Transactions on Power Delivery](https://ieee-pes.org/publications/transactions-on-power-delivery/), vol. 39, no. 2, pp. 1314-1317, April 2024. [**Author's PDF version in Zenodo**](https://zenodo.org/records/10629711/files/TPWRD3359410.pdf?download=1). [Dataset in Zenodo](https://zenodo.org/records/10629711). [Extra files in Zenodo](https://zenodo.org/records/10683771). [Dataset at IEEE DataPort](https://ieee-dataport.org/documents/lossless-and-stateless-compression-iec-61850-sampled-values-flows-0). (Journal Impact Factor 2023: 3.8, ENGINEERING, ELECTRICAL & ELECTRONIC 107/352, Q2. **2024 Journal Impact Factor pending**). doi: [10.1109/TPWRD.2024.3359410](https://doi.org/10.1109/TPWRD.2024.3359410). JCR #22.


#### International Conferences

Adrián Alarcon Becerra, **Jose Saldana**, "Effective Integration of EMTP/ATP Simulations with Protection Devices through GOOSE Communication: A Tool for Virtual Validation of Protection Systems," [European EMTP-ATP Users Group](https://www.atp-emtp.org/nextconf.php), [EEUG](https://www.atp-emtp.org/) Meeting, Rome, October 23-25, 2024.

Anibal Antonio Prada Hurtado, Eduardo Martínez Carrasco, **Jose Saldana**, Carlos Albero Castillón, Konstantinos F. Krommydas,Christos-Spyridon G. Karavas, Konstantinos A. Plakas, Efthimia Chassioti,Ioannis Moraitis, "[Development and Implementation of a WAMPAC Algorithm for DetectingReal-Time Voltage Instability Phenomena in Electric Power Systems](https://www.researchgate.net/publication/382625519_Development_and_Implementation_of_a_WAMPAC_Algorithm_for_Detecting_Real-Time_Voltage_Instability_Phenomena_in_Electric_Power_Systems#fullTextFileContent)," [CIGRE 2024 Paris](https://session.cigre.org/fileadmin/user_upload/2024_TECHNICAL_PROGRAMME_JUNE_2024.pdf), Paper ID – 10630B5 PROTECTION & AUTOMATIONPS2 - Acceptance, commissioning, and field testing for protection, automation andcontrol systems. IC #43.

### 2023

#### International Journals

**Jose Saldana**, Aníbal Antonio Prada Hurtado, Eduardo Martinez Carrasco, Yasmina Galve, Jesús Torres, "[_Fast and Reliable Sending of Generic Object Oriented Substation Event Frames between Remote Locations over Loss-Prone Networks_](https://www.mdpi.com/1424-8220/23/21/8879)," in [Sensors](https://www.mdpi.com/journal/sensors) 2023, 23(21), 8879. doi: [10.3390/s23218879](https://doi.org/10.3390/s23218879), [**Open Access**](https://www.mdpi.com/1424-8220/23/21/8879/pdf). (Journal Impact Factor 2023: 3.4, CHEMISTRY, ANALYTICAL Q2, 34/106; ENGINEERING, ELECTRICAL & ELECTRONIC 122/352, Q2; INSTRUMENTS & INSTRUMENTATION 24/76, Q2). JCR #21.

#### International Conferences

Maria Teresa Villen, Maria Paz Comech, Eduardo Martinez, Roberto Matute, M.A. Oliván, Anibal Prada, **Jose Saldana**, "[_A computer-assisted Faulted Phase Selection Algorithm for dealing with the effects of Renewable Resources in Smart grids_](https://zenodo.org/records/8402195)," Proc. [NEIS 2023 - Conference on Sustainable Energy Supply and Energy Storage Systems](https://neis-conference.com/). Hamburg, September 4-5, 2023. [**PDF in Zenodo**](https://zenodo.org/records/8402195/files/A%20computer-assisted%20Faulted%20Phase%20Selection%20Algorithm%20for%20dealing.pdf?download=1). IC #42.


### 2022

#### International Journals

J. G. Fornás, E. H. Jaraba, A. L. Estopiñan and **Jose Saldana**, "[_Detection and Classification of Fault Types in Distribution Lines by Applying Contrastive Learning to GAN Encoded Time-Series of Pulse Reflectometry Signals_](https://ieeexplore.ieee.org/document/9919840)," in [IEEE Access](http://ieeeaccess.ieee.org/), vol. 10, pp. 110521-110536, 2022, doi: [10.1109/ACCESS.2022.3214994](https://doi.org/10.1109/ACCESS.2022.3214994), [**Open Access**](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9919840). (Journal Impact Factor 2022: 3.9, COMPUTER SCIENCE, INFORMATION SYSTEMS, Q2, 73/158; ENGINEERING, ELECTRICAL & ELECTRONIC Q2, 100/275; TELECOMMUNICATIONS Q2, 41/88). JCR #20.

Prada Hurtado, Aníbal Antonio, Eduardo Martinez Carrasco, Maria Teresa Villén Martínez, and **Jose Saldana**. 2022. "[_Application of IIA Method and Virtual Bus Theory for Backup Protection of a Zone Using PMU Data in a WAMPAC System_](https://www.mdpi.com/1996-1073/15/9/3470)," [Energies](https://www.mdpi.com/journal/energies) 15, no. 9: 3470. doi: [10.3390/en15093470](https://doi.org/10.3390/en15093470), [**Open Access**](https://www.mdpi.com/1996-1073/15/9/3470/pdf?version=1652252361). (Journal Impact Factor 2022: 3.2, ENERGY & FUELS Q3, 78/115). JCR #19.

#### International Conferences

Konstantinos F. Krommydas, Christos-Spyridon G. Karavas, Konstantinos A. Plakas, Dimitrios Melissaris, Christos N. Dikaiakos, Ioannis Moraitis, Anibal Prada Hurtado, Marta Bernal Sancho, Eduardo Martinez Carrasco, **Jose Saldana**, Virgilio De Andrade, Agustin Padilla, Dalibor Brnobic, Toni Petrinic, Anastasis Tzoumpas, "[_Design of a WAMPAC System for Implementation in the Greek Transmission System_](https://ieeexplore.ieee.org/document/9960698)," [2022 IEEE PES Innovative Smart Grid Technologies Conference Europe (ISGT-Europe)](https://attend.ieee.org/isgt-europe-2022/), 2022, pp. 1-6, doi: [10.1109/ISGT-Europe54678.2022.9960698](https://doi.org/10.1109/ISGT-Europe54678.2022.9960698). IC #41.

E. Damián Gutiérrez Mlot, **Jose Saldana** and R. J. Rodríguez, "[_Towards a Testbed for Critical Industrial Systems: SunSpec Protocol on DER Systems as a Case Study_](https://ieeexplore.ieee.org/document/9921522)," [2022 IEEE 27th International Conference on Emerging Technologies and Factory Automation (ETFA)](https://2022.ieee-etfa.org/), 2022, pp. 1-4, doi: [10.1109/ETFA52439.2022.9921522](https://doi.org/10.1109/ETFA52439.2022.9921522). IC #40.

Aníbal Prada, Eduardo Martinez, **Jose Saldana**, Marta Bernal, Noemi Galan, "[_Testing of Power Oscillation detection algorithm using a Real-Time PMU laboratory_](https://www.cigre.org/userfiles/files/TECHNICAL%20PROGRAMME%2022%20August%202022.pdf)" [CIGRE 2022. Paris, France](https://www.cigre.org/event/session/2022/FR/paris/2022-paris-session). 28 August – 02 September 2022. IC #39.

#### Spanish Conferences

**Jose Saldana**, Esteban Damian Gutierrez Mlot, Anibal Antonio Prada Hurtado, "[_Testbed para Defensa ante Amenazas Híbridas contra Redes Eléctricas_](https://publicaciones.defensa.gob.es/ix-congreso-nacional-de-i-d-en-defensa-y-seguridad-15-16-y-17-de-noviembre-de-2022-resumenes-libros-pdf.html)", [IX Congreso Nacional de I+D en Defensa y Seguridad (DESEi+d 2022)](https://www.tecnologiaeinnovacion.defensa.gob.es/es-es/Presentacion/deseid_2022/Paginas/Defensa.aspx), Pontevedra, Spain, 15-17 Nov, 2022. [PDF in Zenodo](https://zenodo.org/record/7627943#.Y-YHS3aCGUl). SPC #20.

### 2021

#### International Journals

**Jose Saldana**, O. Topal, J. Ruiz-Mas and J. Fernández-Navajas, "[_Finding the Sweet Spot for Frame Aggregation in 802.11 WLANs_](https://ieeexplore.ieee.org/document/9291427)," in [IEEE Communications Letters](https://www.comsoc.org/publications/journals/ieee-comml), vol. 25, no. 4, pp. 1368-1372, April 2021, doi: [10.1109/LCOMM.2020.3044231](https://doi.org/10.1109/LCOMM.2020.3044231). [**Author's PDF version in arxiv.org**](https://arxiv.org/pdf/2103.05024). (Journal Impact Factor 2021: 3.553, TELECOMMUNICATIONS Q2, 38/88). JCR #18.

**Jose Saldana**, José Ruiz-Mas, Julián Fernández-Navajas, José Luis Salazar Riaño, Jean-Philippe Javaudin, Jean-Michel Bonnamy, Maël Le Dizes, "[_Attention to Wi-Fi Diversity: Resource Management in WLANs With Heterogeneous APs_](https://ieeexplore.ieee.org/document/9314137)," in [IEEE Access](http://ieeeaccess.ieee.org/), vol. 9, pp. 6961-6980, 2021, doi: [10.1109/ACCESS.2021.3049180](https://doi.org/10.1109/ACCESS.2021.3049180). **Open Access**. (Journal Impact Factor 2021: 3.476, COMPUTER SCIENCE, INFORMATION SYSTEMS, Q2, 79/164; ENGINEERING, ELECTRICAL & ELECTRONIC Q2, 105/276; TELECOMMUNICATIONS Q2, 43/93). JCR #17.

### 2020

#### International Journals

J. D. de Hoz, **Jose Saldana**, J. Fernández-Navajas and J. Ruiz-Mas, "[_Decoupling security from applications in CoAP-based IoT devices_](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8890675)," in [IEEE Internet of Things Journal](http://ieee-iotj.org/), vol. 7, no. 1, pp. 467-476, Jan. 2020. [IEEEXplore](https://ieeexplore.ieee.org/document/8890675). doi: [10.1109/JIOT.2019.2951306](https://doi.org/10.1109/JIOT.2019.2951306). (Journal Impact Factor 2020: 9.471, COMPUTER SCIENCE, INFORMATION SYSTEMS Q1, 6/161; ENGINEERING, ELECTRICAL & ELECTRONIC Q1, 15/273; TELECOMMUNICATIONS Q1, 6/91). JCR #16.

#### International Conferences

J.L. Salazar, **Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Guillermo Azuara G (2021), "[_Short Message Multichannel Broadcast Encryption_](https://link.springer.com/chapter/10.1007/978-3-030-57805-3_12)". In: Herrero A., Cambra C., Urda D., Sedano J., Quintian H., Corchado E. (eds) 13th International Conference on Computational Intelligence in Security for Information Systems (CISIS 2020). CISIS 2019. Advances in Intelligent Systems and Computing, vol 1267. Springer, Cham. [SpringerLink](https://link.springer.com/chapter/10.1007/978-3-030-57805-3_12). doi: [10.1007/978-3-030-57805-3_12](https://doi.org/10.1007/978-3-030-57805-3_12). [**Author's PDF version at Unizar repository**](https://zaguan.unizar.es/record/106745/files/texto_completo.pdf). IC #38.

### 2019

#### International Journals

J. D. de Hoz, **Jose Saldana**, J. Fernández-Navajas and J. Ruiz-Mas, "[_IoTsafe, Decoupling Security from Applications for a Safer IoT_](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8649577)," in [IEEE Access](http://ieeeaccess.ieee.org/). vol. 7, pp. 29942-29962, 2019. [IEEEXplore](https://ieeexplore.ieee.org/document/8649577). doi: [10.1109/ACCESS.2019.2900939](https://doi.org/10.1109/ACCESS.2019.2900939), **Open Access**. (Journal Impact Factor 2019: 3.745, COMPUTER SCIENCE, INFORMATION SYSTEMS, Q1, 35/156; ENGINEERING, ELECTRICAL & ELECTRONIC Q1, 61/266). JCR #15.

I. Quintana, L. Sequeira, J. Fernández, J. Ruiz and **Jose Saldana**, "[_Minimizing the Impact of P2P-TV Applications in Access Links_](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8863163)," in [IEEE Latin America Transactions](http://www.ewh.ieee.org/reg/9/etrans/eng/), vol. 17, no. 02, pp. 183-192, February 2019. [IEEEXplore](https://ieeexplore.ieee.org/document/8863163). doi: [10.1109/TLA.2019.8863163](https://doi.org/10.1109/TLA.2019.8863163). (Journal Impact Factor 2019: 0.782, ENGINEERING, ELECTRICAL & ELECTRONIC Q4, 232/275). JCR #14.

### 2018

#### International Journals

Faycal Bouhafs, Michael Mackay, Alessandro Raschella, Qi Shi, Frank den Hartog, **Jose Saldana**, Jose Ruiz-Mas, Julian Fernández-Navajas, Ruben Munilla, Jose Almodovar, Niels van Adrichem, "[_Wi-5: A Programming Architecture for Unlicensed Frequency Bands_](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8535086)," [IEEE Communications Magazine](https://www.comsoc.org/publications/magazines/ieee-communications-magazine), vol. 56, no. 12, pp. 178-185, December 2018. [IEEEXplore](https://ieeexplore.ieee.org/document/8535086). doi: [10.1109/MCOM.2018.1800246](https://doi.org/10.1109/MCOM.2018.1800246). [**Author's PDF version at LJMU repository**](http://researchonline.ljmu.ac.uk/id/eprint/9608/14/Wi-5%20A%20Programming%20Architecture%20for%20Unlicensed%20Frequency%20Bands.pdf). (Journal Impact Factor 2018: 10.356, ENGINEERING, ELECTRICAL & ELECTRONIC Q1, 7/266; TELECOMMUNICATIONS Q1, 4/90). JCR #13.

**Jose Saldana**, Ruben Munilla, Salim Eryigit, Omer Topal, Jose Ruiz Mas, Julian Fernández-Navajas, Luis Sequeira, "[_Unsticking the Wi-Fi Client: Smarter Decisions using a Software Defined Wireless Solution_](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8371599)," in [IEEE Access](http://ieeeaccess.ieee.org/), [vol. 6, pp. 30917-30931](https://ieeexplore.ieee.org/document/8371599/), 2018. doi: [10.1109/ACCESS.2018.2844088](https://doi.org/10.1109/ACCESS.2018.2844088). Electronic ISSN: 2169-3536. **Open access**. (Journal Impact Factor 2018: 4.098, COMPUTER SCIENCE, INFORMATION SYSTEMS Q1, 23/155; ENGINEERING, ELECTRICAL & ELECTRONIC Q1, 52/266; TELECOMMUNICATIONS Q1, 42/116). JCR #12.

#### International Conferences

J. David de Hoz, **Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Rebeca Guerrero Rodriguez, Felix de Jesus Mar Luna, "_SSH as an Alternative to TLS in IoT Environments using HTTP_," [2018 Global Internet of Things Summit (GIoTS)](http://www.globaliotsummit.org/), Bilbao, Spain, 2018, pp. 1-6. [IEEEXplore](https://ieeexplore.ieee.org/document/8534545). doi: [10.1109/GIOTS.2018.8534545](https://doi.org/10.1109/GIOTS.2018.8534545). IC #37.

### 2017

#### International Journals

**Jose Saldana**, Jose Ruiz-Mas, Jose Almodovar, "[_Frame Aggregation in Central Controlled 802.11 WLANs: the Latency vs. Throughput Trade-off_](https://zaguan.unizar.es/record/71214?ln=es)," in [IEEE Communications Letters](https://www.comsoc.org/publications/journals/ieee-comml), [vol.21, no. 11, pp. 2500-2530](http://ieeexplore.ieee.org/document/8013762/), Nov. 2017. ISSN 1089-7798. doi: [10.1109/LCOMM.2017.2741940](http://dx.doi.org/10.1109/LCOMM.2017.2741940). [Open dataset in Zenodo](https://zenodo.org/record/1048974). [**Author's PDF version at Unizar repository**](https://zaguan.unizar.es/record/71214/files/texto_completo.pdf). (Journal Impact Factor 2017: 2.723, TELECOMMUNICATIONS Q2, 31/87). JCR #11.

**Jose Saldana**, Andres Arcia-Moret, Arjuna Sathiaseelan, Bart Braem, Ermanno Pietrosemoli, Marco Zennaro, Javier Simo-Reigadas, Ioannis Komnios, Carlos Rey-Moreno, "[_Alternative Networks: Towards a Global Access to the Internet for All_](https://zaguan.unizar.es/record/70817?ln=es)," [IEEE Communications Magazine](https://www.comsoc.org/publications/magazines/ieee-communications-magazine), vol. 55, no. 9, pp. 187-193, 2017. [IEEEXplore](http://ieeexplore.ieee.org/document/7934183/). doi [10.1109/MCOM.2017.1600663](http://dx.doi.org/10.1109/MCOM.2017.1600663). [Open dataset in Zenodo](https://zenodo.org/record/898088). [**Author's PDF version at Unizar repository**](https://zaguan.unizar.es/record/70817/files/texto_completo.pdf). (Journal Impact factor 2017: 9.270, TELECOMMUNICATIONS Q1, 2/87; ENGINEERING, ELECTRICAL & ELECTRONIC Q1, 4/260). JCR #10.

Luis Sequeira, Juan Luis de la Cruz, Jose Ruiz-Mas, **Jose Saldana**, Julian Fernández-Navajas, Jose Luis Almodovar, "[_Building a SDN Enterprise WLAN Based On Virtual APs_](https://arxiv.org/abs/2010.13861)," [IEEE Communications Letters](https://www.comsoc.org/publications/journals/ieee-comml), [vol. 21, no. 2, pp. 374-377, Feb. 2017](http://ieeexplore.ieee.org/document/7727996/). ISSN 1089-7798. doi [10.1109/LCOMM.2016.2623602](http://dx.doi.org/10.1109/LCOMM.2016.2623602). [Open dataset in Zenodo](https://zenodo.org/record/292445#.WK18IPnJzIV). [**Author's PDF version in arxiv.org**](https://arxiv.org/pdf/2010.13861). (Journal Impact Factor 2017: 2.723, TELECOMMUNICATIONS Q2, 31/87). JCR #9.

#### Spanish Conferences

Julian Fernández Navajas, Luis Sequeira Villarreal, Jose Ruiz Mas, **Jose Saldana**, "_Mejora de la Calidad en Redes WLAN Coordinadas a través de SDWN_", [XIII Jornadas de Ingeniería Telematica (JITEL 2017)](http://jlloret.webs.upv.es/jitel2017/index.html), Valencia (Spain), 27-29 de Sept 2017. ISBN: 978-84-9048-595-8. doi: [10.4995/JITEL2017.2017.6564](http://dx.doi.org/10.4995/JITEL2017.2017.6564). SPC #19.

### 2016

#### International Journals

**Jose Saldana**, "[_On the effectiveness of an optimization method for the traffic of TCP-based multiplayer online games_](https://link.springer.com/article/10.1007/s11042-015-3001-y)," [Multimedia Tools and Applications](http://www.springer.com/computer/information+systems+and+applications/journal/11042) (Dec. 2016) Vol. 75: pp 17333-17374. doi: [10.1007/s11042-015-3001-y](http://dx.doi.org/10.1007/s11042-015-3001-y), Springer. [**Author's PDF version at Unizar repository**](https://zaguan.unizar.es/record/56834/files/texto_completo.pdf). (Journal Impact Factor 2016: 1.530; COMPUTER SCIENCE, THEORY & METHODS Q2, 45/104; COMPUTER SCIENCE, SOFTWARE ENGINEERING Q2, 48/106; COMPUTER SCIENCE, INFORMATION SYSTEMS Q3, 87/146; ENGINEERING, ELECTRICAL & ELECTRONIC Q3, 148/262). JCR #8.


#### International Conferences

Mirko Suznjevic, **Jose Saldana**, Maja Matijasevic, Matko Vuga, "[_Impact of Simplemux Traffic Optimisation on MMORPG QoE_](http://www.isca-speech.org/archive/PQS_2016/abstracts/3.html)," presented at the [PQS 2016, 5th ISCA/DEGA Workshop on Perceptual Quality of Systems](http://www.isca-speech.org/archive/PQS_2016/), Berlin, Germany, 29-31 Aug 2016. ISSN: 2312-2846. doi: [10.21437/PQS.2016-4](http://dx.doi.org/10.21437/PQS.2016-4). [PDF version](http://www.isca-speech.org/archive/PQS_2016/pdfs/3.pdf). [Open dataset in Zenodo](https://zenodo.org/record/292625#.WK187_nJzIU). IC #36.

J. David de Hoz, **Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Rebeca Guerrero Rodríguez, Felix de Jesus Mar Luna, Raul Ivan Herrera Gonzalez, "[_Leveraging on Digital Signage Networks to Bring Connectivity to IoT Devices_](author_versions/leveragin_ds_to_iot.pdf)," [Tecnia](http://revistas.uni.edu.pe/index.php/tecnia/index). [Vol 26, n. 1](http://revistas.uni.edu.pe/index.php/tecnia/article/view/117) (2016). ISSN 0375-7765. doi: [10.21754/tecnia.v26i1.117](http://dx.doi.org/10.21754/tecnia.v26i1.117). Also presented in [Telcon UNI 2015](http://cct-uni.pe/telcon/), Lima, Peru, Oct. 2015. [Open dataset in Zenodo](http://dx.doi.org/10.5281/zenodo.35383). IC #35.

### 2015

#### International Journals

**Jose Saldana**, David de Hoz, Julian Fernández-Navajas, Jose Ruiz-Mas, Fernando Pascual, Diego R. Lopez, David Florez, J. A. Castell, M. Nunez. "[_Small-Packet Flows in Software Defined Networks: Traffic Profile Optimization_](https://zaguan.unizar.es/record/56191?ln=es)". Journal Of Networks, 10(4), 2015, 176-187. doi:10.4304/jnw.10.4.176-187. [**Author's PDF version at Unizar repository**](https://zaguan.unizar.es/record/56191/files/texto_completo.pdf).


#### Book chapters

**Jose Saldana**, Mirko Suznjevic, "[_QoE and Latency Issues in Networked Games_](https://link.springer.com/referenceworkentry/10.1007/978-981-4560-52-8_23-1)," [Handbook of Digital Games and Entertainment Technologies](http://www.springer.com/us/book/9789814560498), Published online 22 August 2015, Springer. doi: [10.1007/978-981-4560-52-8_23-1](http://dx.doi.org/10.1007/978-981-4560-52-8_23-1). [**Author's PDF version in Researchgate**](https://www.researchgate.net/publication/292152780_QoE_and_Latency_Issues_in_Networked_Games).

#### International Conferences

**Jose Saldana**, Juan Luis de la Cruz, Luis Sequeira, Julian Fernández-Navajas, Jose Ruiz-Mas, "[_Can a Wi-Fi WLAN Support a First Person Shooter?_](http://ieeexplore.ieee.org/document/7383001/)," [NetGames 2015, The 14th International Workshop on Network and Systems Support for Games](http://netgames2015.fer.hr/), Zagreb, Croatia, December 3-4, 2015. [Presentation](http://es.slideshare.net/josemariasaldana/can-a-wifi-wlan-support-a-first-person-shooter). [Open dataset in Zenodo](http://dx.doi.org/10.5281/zenodo.35433). [ACM](http://dl.acm.org/citation.cfm?id=2984090). [**Author's PDF version in Arxiv**](https://arxiv.org/pdf/2007.16071). IC #34.

**Jose Saldana**, Ignacio Forcen, Julian Fernández-Navajas, Jose Ruiz-Mas, "[_Improving Network Efficiency with Simplemux_](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=7363105)," [IEEE CIT 2015](http://cse.stfx.ca/~cit2015/), International Conference on Computer and Information Technology, pp. 446-453, 26-28 October 2015, Liverpool, UK. [Presentation](http://es.slideshare.net/josemariasaldana/improving-network-efficiency-with-simplemux). [Open dataset in Zenodo](http://dx.doi.org/10.5281/zenodo.35246). doi: [10.1109/CIT/IUCC/DASC/PICOM.2015.64](http://dx.doi.org/10.1109/CIT/IUCC/DASC/PICOM.2015.64). [**Author's PDF version in Researchgate**](https://www.researchgate.net/publication/304674195_Improving_Network_Efficiency_with_Simplemux). IC #33.


### 2014

#### International Journals

Luis Sequeira, Julian Fernández-Navajas, **Jose Saldana**, "[_The Effect of the Buffer Size in QoS for Multimedia and bursty Traffic: When an Upgrade Becomes a Downgrade_](http://www.itiis.org/digital-library/manuscript/834)", [KSII Transactions on Internet and Information Systems](http://www.itiis.org/), Vol. 8, Issue 9, pp 3159 - 3176. Sep. 2014. doi: [10.3837/tiis.2014.09.012](http://dx.doi.org/10.3837/tiis.2014.09.012). **[Open access PDF](https://www.itiis.org/journals/tiis/digital-library/manuscript/file/20608/TIISVol8No9-12.pdf)**. (Journal Impact Factor 2014: 0.561, TELECOMMUNICATIONS Q4, 65/77; COMPUTER SCIENCE, INFORMATION SYSTEMS Q4, 118/139). JCR #7.

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo Viruete Navarro, Luis Casadesus, "[_Online FPS Games: Effect of Router Buffer and Multiplexing Techniques on Subjective Quality Estimators_](https://link.springer.com/article/10.1007/s11042-012-1309-4)," [Multimedia Tools and Applications](http://www.springer.com/computer/information+systems+and+applications/journal/11042), [Volume 71, Issue 3](http://link.springer.com/journal/11042/71/3/page/1), pp 1823-1856, August 2014, Springer. doi: [10.1007/s11042-012-1309-4](http://dx.doi.org/10.1007/s11042-012-1309-4). [**Author's PDF version in Researchgate**](https://www.researchgate.net/publication/257627517_Online_FPS_games_Effect_of_router_buffer_and_multiplexing_techniques_on_subjective_quality_estimators). (Journal Impact Factor 2016: 1.346; COMPUTER SCIENCE, THEORY & METHODS Q2, 32/102; COMPUTER SCIENCE, SOFTWARE ENGINEERING Q2, 36/104; COMPUTER SCIENCE, INFORMATION SYSTEMS Q2, 52/139; ENGINEERING, ELECTRICAL & ELECTRONIC Q2, 114/249). JCR #6.

Jose Luis Tornos, Jose Luis  Salazar, Joan Josep Piles, **Jose Saldana**, Luis Casadesus, Jose Ruiz-Mas y Julian Fernández-Navajas, "[_An eVoting System Based on Ring Signatures_](http://macrothink.org/journal/index.php/npa/article/download/5390/4824)," [Network Protocols and Algorithms](http://www.macrothink.org/journal/index.php/npa), [vol. 6, Issue 2](http://macrothink.org/journal/index.php/npa/issue/view/295/showToc), pp 38-54, 2014. ISSN: 1943-3581. doi: [10.5296/npa.v6i2.5390](http://dx.doi.org/10.5296/npa.v6i2.5390). **Open access**.

Luis Sequeira, Julian Fernández-Navajas, **Jose Saldana**, Jose Ramon Gallego, Maria Canales, "[_Describing the Access Network by means of Router Buffer Modelling: a New Methodology_](https://www.hindawi.com/journals/tswj/2014/238682/)", [The Scientific World Journal](https://www.hindawi.com/journals/tswj/), vol. 2014, Article ID 238682, 9 pages, 2014. doi: [10.1155/2014/238682](http://dx.doi.org/10.1155/2014/238682). **Open access**.

Carlos Fernández, **Jose Saldana**, Julian Fernández-Navajas, Luis Sequeira, Luis Casadesus, "[_Video conferences through the Internet: How to Survive in a Hostile Environment_](http://downloads.hindawi.com/journals/tswj/2014/860170.pdf)," [The Scientific World Journal](https://www.hindawi.com/journals/tswj/), vol. 2014, Article ID 860170, 13 pages, 2014. doi: [10.1155/2014/860170](http://dx.doi.org/10.1155/2014/860170). **Open access**.

Mirko Suznjevic, **Jose Saldana**, Maja Matijasevic, Julian Fernández-Navajas, Jose Ruiz-Mas, "[_Analyzing the effect of TCP and server population on massively multiplayer games_](https://www.hindawi.com/journals/ijcgt/2014/602403/)," [International Journal of Computer Games Technology](https://www.hindawi.com/journals/ijcgt/), vol. 2014, Article ID 602403, 17 pages, 2014. doi: [10.1155/2014/602403](http://dx.doi.org/10.1155/2014/602403). **Open access**.


#### International Conferences

**Jose Saldana**, Fernando Pascual, David de Hoz, Julian Fernández-Navajas, Jose Ruiz-Mas, Diego R. Lopez, David Florez, Juan A. Castell, Manuel Nunez, "[_Optimization of Low-efficiency Traffic in OpenFlow Software Defined Networks_](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6879992)," Proc. [International Symposium on Performance Evaluation of Computer and Telecommunication Systems SPECTS 2014](http://atc.udg.edu/SPECTS2014/), pp. 550-555, Monterey, CA, USA, July 6-10, 2014. [Presentation](http://es.slideshare.net/josemariasaldana/japan-spects2014-presentationv1). ISBN:1-56555-354-3. doi: [10.1109/SPECTS.2014.6879992](http://dx.doi.org/10.1109/SPECTS.2014.6879992).[**Author's PDF version**](author_versions/japan_spects_2014_in_proc.pdf). IC #32.

**Jose Saldana**, Gustavo Marfia, Marco Roccetti, "[_Everything you always wanted to know about playing a FPS game on a car_](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6849026)," Proc. [Communications and Networking IEEE BlackSeaCom 2014, IEEE International Black Sea Conference on](http://www.ieee-blackseacom.org/2014/), pp.139,143, 27-30 May 2014,Chisinau, Moldova. ISBN: 978-1-4799-4067-7. doi: [10.1109/BlackSeaCom.2014.6849026](http://dx.doi.org/10.1109/BlackSeaCom.2014.6849026). [**Author's PDF version**](author_versions/blacksea_2014_in_proc.pdf). IC #31.

Luis Sequeira, Julian Fernández-Navajas, **Jose Saldana**, "[_Characterization of Real Internet Paths by Means of Packet Loss Analysis_](http://www.thinkmind.org/index.php?view=article&articleid=icds_2014_4_10_10153)," [ICDS 2014, The Eighth International Conference on Digital Society](http://www.thinkmind.org/index.php?view=instance&instance=ICDS+2014). Barcelona, March 23, 2014, pp 80-85. ISBN 978-1-61208-324-7. IC #30.

**Jose Saldana**, "[_The Effect of Multiplexing Delay on MMORPG TCP Traffic Flows_](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6866578)," [Consumer Communications and Networking Conference, CCNC 2014](http://ccnc2014.ieee-ccnc.org/). Las Vegas, January 10, 2014, pp 447-452. ISBN 978-1-4799-2356-4. [Presentation](http://es.slideshare.net/josemariasaldana/sawtooth-ccnc2014-presentationv2). doi: [10.1109/CCNC.2014.6866578](http://dx.doi.org/10.1109/CCNC.2014.6866578). [**Author's PDF version**](author_versions/sawtooth_ccnc_2014_in_proc.pdf). IC #29.

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas "[_Can We Multiplex ACKs without Harming the Performance of TCP?_](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6994400)," [Consumer Communications and Networking Conference, CCNC 2014](http://ccnc2014.ieee-ccnc.org/). Las Vegas, January 10, 2014, pp 503-504. ISBN 978-1-4799-2356-4. [Presentation](http://es.slideshare.net/josemariasaldana/berlin-ccnc2014-presentationv3). doi: [10.1109/CCNC.2014.6994400](http://dx.doi.org/10.1109/CCNC.2014.6994400). [**Author's PDF version**](author_versions/berlin_ccnc_2014_in_proc.pdf). IC #28.


### 2013

#### International Journals

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Dan Wing, Muthu A. M. Perumal, Michael Ramalho, Gonzalo Camarillo, Fernando Pascual, Diego R. Lopez, Manuel Nunez, David Florez, Juan A. Castell, Tomaso de Cola, Matteo Berioli, "[_Emerging Real-time Services: Optimizing Traffic by Smart Cooperation in the Network_](https://ieeexplore.ieee.org/document/6658664)," [IEEE Communications Magazine](https://www.comsoc.org/publications/magazines/ieee-communications-magazine), [Vol. 51, n. 11, pp 127-136, Nov. 2013](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6658664). doi: [10.1109/MCOM.2013.6658664](http://dx.doi.org/10.1109/MCOM.2013.6658664). [**Author's PDF version**](author_versions/yoda_commag_2013.pdf). (Journal Impact Factor 2013: 4.460, TELECOMMUNICATIONS Q1, 3/78; ENGINEERING, ELECTRICAL & ELECTRONIC Q1, 10/248). JCR #5.

#### International Conferences

Idelkys Quintana-Ramirez, **Jose Saldana**, Jose Ruiz-Mas, Luis Sequeira, Julian Fernández-Navajas, Luis Casadesus, "[_Optimization of P2P-TV Traffic by Means of Header Compression and Multiplexing_](https://ieeexplore.ieee.org/document/6671891)", [SoftCOM 2013](http://marjan.fesb.hr/SoftCOM/2013/index.html), Split, Croatia, September 18-20, 2013. ISBN 978-953-290-041-5. doi: [10.1109/SoftCOM.2013.6671891](https://doi.org/10.1109/SoftCOM.2013.6671891). [**Author's PDF version**](author_versions/p2ptv_softcom_2013_in_proc.pdf). IC #27.

Luis Sequeira, Julian Fernández-Navajas, **Jose Saldana**, "[_Characterization of the Buffers in Real Internet Paths_](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6595756&isnumber=6595732)," Proc. [International Symposium on Performance Evaluation of Computer and Telecommunication Systems SPECTS 2013](http://atc.udg.edu/SPECTS2013/), Toronto, Canada, July 2013, pp 666-671. ISBN 1-56555-352-7. [**Author's PDF version**](author_versions/characterization_spects2013_in_proc.pdf). IC #26.

Luis Sequeira, Julian Fernández-Navajas, Luis Casadesus, **Jose Saldana**, Idelkys Quintana, Jose Ruiz-Mas, "[_The Influence of the Buffer Size in Packet Loss for Competing Multimedia and Bursty Traffic_](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6595753&isnumber=6595732)," Proc. [International Symposium on Performance Evaluation of Computer and Telecommunication Systems SPECTS 2013](http://atc.udg.edu/SPECTS2013/), Toronto, Canada, July 2013, pp 645-652. ISBN 1-56555-352-7. [**Author's PDF version**](author_versions/influence_spects2013_in_proc.pdf). IC #25.

**Jose Saldana**, Luigi Iannone, Diego R. Lopez, Julian Fernández-Navajas, Jose Ruiz-Mas, "[_Enhancing Throughput Efficiency via Multiplexing and Header Compression over LISP Tunnels_](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6649436&isnumber=6649166)". In Proc. [Second IEEE Workshop on Telecommunication Standards: From Research to Standards](http://www.research2standards.net/), Collocated with [IEEE ICC 2013](http://www.ieee-icc.org/2013/), Budapest, Hungary. ISBN 9781467357524. [Presentation](http://es.slideshare.net/josemariasaldana/budapest-icc-2013presentation). doi: [10.1109/ICCW.2013.6649436](http://dx.doi.org/10.1109/ICCW.2013.6649436). [**Author's PDF version**](author_versions/budapest_ICC_2013_in_proc.pdf). IC #24.


#### Spanish Conferences

Jose Javier Serrano, Julian Fernández-Navajas, **Jose Saldana**, "_Aplicación al Ámbito Académico de un entorno de Simulación/Emulación de Arquitecturas de Red_," [III Jornadas de Innovación Educativa en Ingeniería Telematica (JIE) 2013](http://dtstc.ugr.es/jitel2013/jie.php), JITEL 2013, pp 577 - 584. Granada, Spain, October 28-30, 2013. ISBN-13: 978-84-616-5597-7. [**Author's PDF version**](author_versions/dynamips_jie_2013_in_proc.pdf). SPC #18.

Idelkys Quintana, **Jose Saldana**, Jose Ruiz Mas, Luis Sequeira, Julian Fernández Navajas, Luis Casadesus, "_Optimización del Trafico P2P-TV mediante el uso de Técnicas de Compresión y Multiplexion_,"  [Jornadas de Ingeniería Telematica JITEL 2013](http://dtstc.ugr.es/jitel2013), pp 345-350, Granada, Spain, October 28-30, 2013. ISBN-13: 978-84-616-5597-7. [**Author's PDF version**](author_versions/p2p-tv_jitel_2013_in_proc.pdf). SPC #17.

### 2012

#### International Journals

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Jenifer Murillo, Eduardo Viruete, Jose I. Aznar, "_[Evaluating the Influence of Multiplexing Schemes and Buffer Implementation on Perceived VoIP Conversation Quality](https://www.sciencedirect.com/science/article/abs/pii/S1389128612000618?via%3Dihub)_," [Computer Networks](http://www.journals.elsevier.com/computer-networks/) (Elsevier), Volume 56, Issue 7, Pages 1893-1919, May 2012. doi: [10.1016/j.comnet.2012.02.004](http://dx.doi.org/10.1016/j.comnet.2012.02.004). [**Author's PDF version**](author_versions/computer_networks_2012_in_proc.pdf). (Journal Impact Factor 2012: 1.231, COMPUTER SCIENCE, HARDWARE & ARCHITECTURE Q2, 16/50; TELECOMMUNICATIONS Q2 18/78; COMPUTER SCIENCE, INFORMATION SYSTEMS Q2, 47/132; ENGINEERING, ELECTRICAL & ELECTRONIC Q2, 108/243). JCR #4.

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Luis Casadesus, "_[Online Games Traffic Multiplexing: Analysis and Effect in Access Networks](https://itiis.org/digital-library/20214)_," [KSII Transactions on Internet and Information Systems](https://itiis.org/), [Volume 6, Issue 11](https://itiis.org/digital-library/publication?volume=6&number=11), pp 2920 - 2939. Nov. 30, 2012. doi: [10.3837/tiis.2012.11.010](http://dx.doi.org/10.3837/tiis.2012.11.010). **[Open access PDF](https://itiis.org/journals/tiis/digital-library/manuscript/file/20214/TIISVol6,No11-10.pdf)**. (Journal Impact Factor 2012: 0.560, TELECOMMUNICATIONS Q3, 54/78; COMPUTER SCIENCE, INFORMATION SYSTEMS Q3, 95/132). JCR #3.


#### International Conferences

**Jose Saldana**, Gustavo Marfia, Marco Roccetti, "_[Satisfying the Hunger for Mobile Online Games: Providing Quality Time in Vehicular Scenarios](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?tp=&arnumber=6404019)_,'' Proc. ACM/IEEE 11th Annual Workshop on Network and Systems Support for Games (NetGames'12), Venice, November 2012. ISBN: 978-1-4673-4577-4. ISSN: 2156-8138. doi: [10.1109/NetGames.2012.6404019](http://dx.doi.org/10.1109/NetGames.2012.6404019). [**Author's PDF version**](author_versions/vehicle_prediction_2012_in_proc.pdf). IC #23.

**Jose Saldana**, Gustavo Marfia, Marco Roccetti, "_[First Person Shooters on the Road: Leveraging on APs and Vanets for a Quality Gaming Experience](http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6402812)_," Proc. IFIP [Wireless Days 2012](http://www.wireless-days.org/). ISBN: 978-1-4673-4403-6. ISSN: 2156-9711. doi: [10.1109/WD.2012.6402812](http://dx.doi.org/10.1109/WD.2012.6402812). [Presentation](http://es.slideshare.net/josemariasaldana/wireless-days-presentationv9). [**Author's PDF version**](author_versions/vehicle_coverage_2012_in_proc.pdf). IC #22.

Luis Casadesus, Julian Fernández-Navajas, Luis Sequeira, Idelkys Quintana, **Jose Saldana**, Jose Ruiz-Mas, "[_IPTV Quality assessment system_](https://dl.acm.org/doi/10.1145/2382016.2382026)," [IFIP/ACM LANC 2012 7th Latin America Networking Conference](http://lanc2012.ufpa.br/) 2012, pp. 52-58.  ISBN: 978-1-4503-1750-4. doi: [10.1145/2382016.2382026](http://dx.doi.org/10.1145/2382016.2382026). [**Author's PDF version**](author_versions/lanc_iptv_2012_in_proc.pdf). IC #21.

Luis Sequeira, Idelkys Quintana, **Jose Saldana**, Luis Casadesus, Julian Fernández-Navajas, Jose Ruiz-Mas  "[_The Utility of Characterizing the Buffer of Network Devices in order to Improve Real-time Interactive Services_](https://dl.acm.org/doi/10.1145/2382016.2382020)," [IFIP/ACM LANC 2012 7th Latin America Networking Conference](http://lanc2012.ufpa.br/) 2012, pp. 19-27. ISBN: 978-1-4503-1750-4. doi: [10.1145/2382016.2382020](http://dx.doi.org/10.1145/2382016.2382020). [**Author's PDF version**](author_versions/lanc_buffer_p2p_in_proc.pdf). IC #20.

**Jose Saldana**, Mirko Suznjevic, Luis Sequeira, Julian Fernández-Navajas, Maja Matijasevic, Jose Ruiz-Mas, "[_The Effect of TCP Variants on the Coexistence of MMORPG and Best-Effort Traffic_](https://ieeexplore.ieee.org/document/6289245)," IEEE ICCCN 2012, 8th International Workshop on Networking Issues in Multimedia Entertainment (NIME'12), Munich, Germany, July 30, 2012. ISBN: 978-1-4673-1543-2. doi: [10.1109/ICCCN.2012.6289245](http://dx.doi.org/10.1109/ICCCN.2012.6289245). [Presentation](http://es.slideshare.net/josemariasaldana/hurry-nime-2012presentation). [**Author's PDF version**](author_versions/hurry_NIME_2012_in_proc.pdf). IC #19.

**Jose Saldana**, Luis Sequeira, Julian Fernández-Navajas, Jose Ruiz-Mas, "[_Traffic Optimization for TCP-based Massive Multiplayer Online Games_](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6267019&isnumber=6267013)," Proc. International Symposium on Performance Evaluation of Computer and Telecommunication Systems SPECTS 2012, July 8-11, 2012, Genoa, Italy. ISBN: 978-1-4673-2235-5. [Presentation](http://es.slideshare.net/josemariasaldana/sergei-spects-2012presentation). [**Author's PDF version**](author_versions/sergei_SPECTS_2012_in_proc.pdf). IC #18.

Luis Sequeira, Julian Fernández-Navajas, **Jose Saldana**, Luis Casadesus, Jose Ruiz-Mas, "[_Empirically Characterizing the Buffer Behaviour of Real Devices_](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6267042&isnumber=6267013)," Proc. International Symposium on Performance Evaluation of Computer and Telecommunication Systems SPECTS 2012, July 8-11, 2012, Genoa, Italy. ISBN: 978-1-4673-2235-5. [Presentation](http://es.slideshare.net/josemariasaldana/buffer-measuring-spects2012presentation). [**Author's PDF version**](author_versions/buffer_measuring_SPECTS_2012_in_proc.pdf). IC #17.

**Jose Saldana**, Dan Wing, Julian Fernández-Navajas, Jose Ruiz-Mas, Muthu A.M. Perumal, Gonzalo Camarillo, "[_Widening the Scope of a Standard: Real Time Flows Tunneling, Compressing and Multiplexing_](https://ieeexplore.ieee.org/document/6364779)," IEEE ICC 2012, Workshop on Telecommunications: from Research to Standards, June 10-11, 2012, Ottawa, Canada. doi: [10.1109/ICC.2012.6364779](http://dx.doi.org/10.1109/ICC.2012.6364779). ISSN : 1550-3607. ISBN : 978-1-4577-2051-2. [Presentation](http://es.slideshare.net/josemariasaldana/ottawa-icc-2012presentation). [**Author's PDF version**](author_versions/ottawa_ICC_2012_in_proc.pdf). IC #16.

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo Viruete Navarro, Luis Casadesus, "[_The Effect of Router Buffer Size on Subjective Gaming Quality Estimators based on Delay and Jitter_](https://ieeexplore.ieee.org/document/6181008)," in Proc. CCNC 2012- 4th IEEE International Workshop on Digital Entertainment, Networked Virtual Environments, and Creative Technology (DENVECT), pp. 502-506, Las Vegas. Jan 2012. ISBN 9781457720697. doi: [10.1109/CCNC.2012.6181008](http://dx.doi.org/10.1109/CCNC.2012.6181008). [Presentation](http://es.slideshare.net/josemariasaldana/ccnc-2012-holepresentation). [**Author's PDF version**](author_versions/ccnc_2012_hole_in_proc.pdf). IC #15.

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo Viruete Navarro, Luis Casadesus, "[_Influence of Online Games Traffic Multiplexing and Router Buffer on Subjective Quality_](https://ieeexplore.ieee.org/document/6181001)," in Proc. CCNC 2012- 4th IEEE International Workshop on Digital Entertainment, Networked Virtual Environments, and Creative Technology (DENVECT), pp. 482-486, Las Vegas. Jan 2012. ISBN 9781457720697. [Presentation](http://es.slideshare.net/josemariasaldana/ccnc-2012-julypresentation). doi: [10.1109/CCNC.2012.6181001](http://dx.doi.org/10.1109/CCNC.2012.6181001). [**Author's PDF version**](author_versions/ccnc_2012_july_in_proc.pdf). IC #14.

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo Viruete Navarro, Luis Casadesus, "[_The Utility of Characterizing Packet Loss as a Function of Packet Size in Commercial Routers_](https://ieeexplore.ieee.org/document/6181115)," in Proc. CCNC 2012, Work in progress papers, pp. 362-363, Las Vegas. Jan 2012. ISBN 9781457720697. [Presentation](http://es.slideshare.net/josemariasaldana/ccnc-2012-augustpresentation). doi: [10.1109/CCNC.2012.6181115](http://dx.doi.org/10.1109/CCNC.2012.6181115). [**Author's PDF version**](author_versions/ccnc_2012_august_in_proc.pdf). IC #13.

#### Spanish Conferences

**Jose Saldana**, Julian Fernández Navajas, Jose Ruiz Mas, Luis A. Casadesus Pazos. "Evaluación de la Calidad Subjetiva de Juegos Online segun el Dispositivo de Acceso". Actas del XXVII Simposium Nacional de la Unión Cientifica Internacional de Radio (URSI 2012). Elche (Spain). Sept. 2012. ISBN: 978-84-695-4327-6. [Presentation](http://es.slideshare.net/josemariasaldana/agujero-ursi-2012presentation). [**Author's PDF version**](author_versions/agujero_URSI_2012_in_proc.pdf). SPC #16.

Luis Sequeira, Julian Fernández Navajas, **Jose Saldana**, Luis A. Casadesus Pazos. "Caracterización de Tecnologias y Dispositivos de Red: Comportamiento de los Buffer". Actas del XXVII Simposium Nacional de la Unión Cientifica Internacional de Radio (URSI 2012). Elche (Spain). Sept. 2012. ISBN: 978-84-695-4327-6. [**Author's PDF version**](author_versions/buffer_measuring_URSI_2012_in_proc.pdf). SPC #15.

Idelkys Quintana, **Jose Saldana**, Jose Ruiz Mas, Julian Fernández Navajas, Luis A. Casadesus Pazos, Luis Sequeira. "Influencia del Buffer del Router en la Distribución de Video P2P-TV". Actas del XXVII Simposium Nacional de la Unión Cientifica Internacional de Radio (URSI 2012). Elche (Spain). Sept. 2012. ISBN: 978-84-695-4327-6. [**Author's PDF version**](author_versions/P2PTV_URSI_2012_in_proc.pdf). SPC #14.

**Jose Saldana**, Julian Fernández Navajas, Jose Ruiz Mas, Luis Sequeira, Luis Casadesus, "Comparison of Multiplexing Policies for FPS Games in terms of Subjective Quality". Proc. II Workshop on Multimedia Data Coding and Transmission 2012, Jornadas Sarteco. Elche (Spain). Sept. 2012. ISBN: 978-84-695-4472-3. [Presentation](http://es.slideshare.net/josemariasaldana/sarteco-2012-presentation). [**Author's PDF version**](author_versions/sarteco_2012_in_proc.pdf). SPC #13.


### 2011

#### International Journals

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Jose I. Aznar, Eduardo Viruete, Luis Casadesus, "[_First Person Shooters: Can a Smarter Network Save Bandwidth without Annoying the Players?_](https://ieeexplore.ieee.org/document/6069728)," [IEEE Communications Magazine](https://www.comsoc.org/publications/magazines/ieee-communications-magazine), vol. 49, no.11, pp. 190-198, November 2011. doi: [10.1109/MCOM.2011.6069728](http://dx.doi.org/10.1109/MCOM.2011.6069728). [**Author's PDF version**](author_versions/commag_nov_2011_jsaldana.pdf). (Journal Impact Factor 2011: 3.785, ENGINEERING, ELECTRICAL & ELECTRONIC Q1, 10/245; TELECOMMUNICATIONS Q1, 3/79). JCR #2.

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Jose I. Aznar, Luis Casadesus, Eduardo Viruete, "[_Comparative of Multiplexing Policies for Online Gaming in terms of QoS Parameters_](https://ieeexplore.ieee.org/document/5992704)," [IEEE Communications Letters](https://www.comsoc.org/publications/journals/ieee-comml), vol.15, no.10, pp.1132-1135, October 2011. doi: [10.1109/LCOMM.2011.080811.111160](http://dx.doi.org/10.1109/LCOMM.2011.080811.111160). [**Author's PDF version**](author_versions/letters_2011_in_proc.pdf). (Journal Impact Factor 2011: 0.982, TELECOMMUNICATIONS Q2, 38/79). JCR #1.

#### International Conferences

Jose I. Aznar, Eduardo Viruete, Julian Fernández-Navajas, Jose Ruiz-Mas, **Jose Saldana**, Luis Casadesus,  "_Business Model Approach for QoE Optimized Service Delivery_". Proc. International Conference on E-Business, Seville, Spain. July 2011. ISBN: 978-989-8425-70-6. [**Author's PDF version**](author_versions/business_ICEB_2011_in_proc.pdf). IC #12.

Jose I. Aznar, Eduardo Viruete, Julian Fernández-Navajas, Jose Ruiz-Mas, **Jose Saldana**, Luis Casadesus, "_Evaluation of Distribution Channels for IP Interactive QoE Based Services_". Proc. International Conference on E-Business, Seville, Spain. July. 2011. ISBN: 978-989-8425-70-6. [**Author's PDF version**](author_versions/evaluation_ICEB_2011_in_proc.pdf). IC #11.

**Jose Saldana**, Jenifer Murillo, Julian Fernández-Navajas, Jose Ruiz-Mas, Jose I. Aznar, Eduardo Viruete,  "[_Bandwidth Efficiency Improvement for Online Games by the use of Tunneling, Compressing and Multiplexing Techniques_](https://ieeexplore.ieee.org/document/5984870)". Proc. International Symposium on Performance Evaluation of Computer and Telecommunication Systems SPECTS 2011, pp.227-234, The Hague, Netherlands, June 2011. ISBN: 978-161-782-309-1. [Presentation](http://es.slideshare.net/josemariasaldana/games-spects-2011presentation). [**Author's PDF version**](author_versions/games_SPECTS_2011_in_proc.pdf). IC #10.

Jenifer Murillo, **Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo Viruete, Jose I. Aznar, "[_Improving Quality in a Distributed IP Telephony System by the use of Multiplexing Techniques_](https://ieeexplore.ieee.org/document/5984847)" .Proc. International Symposium on Performance Evaluation of Computer and Telecommunication Systems SPECTS 2011, pp.54-61, The Hague, Netherlands, June 2011. ISBN: 978-161-782-309-1. [Presentation](http://es.slideshare.net/josemariasaldana/alfa-spects-2011presentation). [**Author's PDF version**](author_versions/alfa_SPECTS_2011_in_proc.pdf). IC #9.

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Jose I. Aznar, Eduardo Viruete, Luis Casadesus,  "[_Influence of the Router Buffer on Online Games Traffic Multiplexing_](http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=06181001)" .Proc. International Symposium on Performance Evaluation of Computer and Telecommunication Systems SPECTS 2011,  pp.253-258, The Hague, Netherlands, June 2011. ISBN: 978-161-782-309-1.  [Presentation](http://es.slideshare.net/josemariasaldana/ciber-spects-2011presentation). [**Author's PDF version**](author_versions/ciber_SPECTS_2011_in_proc.pdf). IC #8.

**Jose Saldana**, Jenifer Murillo, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo Viruete, Jose I. Aznar. "[_QoS and Admission Probability Study for a SIP-based Central Managed IP Telephony System_](http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=05720600)". In Proc. New Technologies, Mobility and Security, NTMS 2011, 5th International Conference on, Paris. Feb. 2011. ISBN: 978-1-4244-8704-2. [Presentation](http://es.slideshare.net/josemariasaldana/sipbased-ntms-2011presentation). [**Author's PDF version**](author_versions/sipbased_NTMS_2011_in_proc.pdf). IC #7.

Jose I. Aznar, Eduardo Viruete, Julian Fernández-Navajas, Jose Ruiz-Mas, **Jose Saldana**, Jenifer Murillo. "[_QMoEs: A Bandwidth Estimation and Monitoring Tool for QoE-Driven Broadband Networks_](http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=5720602)". In Proc. New Technologies, Mobility and Security, NTMS 2011, 5th International Conference on, Paris. Feb. 2011. ISBN: 978-1-4244-8704-2. [**Author's PDF version**](author_versions/QMOES_NTMS_2011_in_proc.pdf). IC #6.

**Jose Saldana**, Jenifer Murillo, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo Viruete, Jose I. Aznar. "[_Evaluation of Multiplexing and Buffer Policies Influence on VoIP Conversation Quality_](https://ieeexplore.ieee.org/document/5766495)" . In Proc. CCNC 2011- 3rd IEEE International Workshop on Digital Entertainment, Networked Virtual Environments, and Creative Technology, pp 1147-1151,  Las Vegas. Jan. 2011. doi: [10.1109/CCNC.2011.5766495](http://dx.doi.org/10.1109/CCNC.2011.5766495). ISBN 9781424487882. [Presentation](http://es.slideshare.net/josemariasaldana/gamma-ccnc-2011presentation). [**Author's PDF version**](author_versions/gamma_CCNC_2011_in_proc.pdf). IC #5.

**Jose Saldana**, Jenifer Murillo, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo Viruete, Jose I. Aznar. "[_Influence of the Distribution of TCRTP Multiplexed Flows on VoIP Conversation Quality_](https://ieeexplore.ieee.org/document/5766573)" . In Proc. CCNC 2011. The 8th Annual IEEE Consumer Communications and Networking Conference - Work in Progress papers, pp 711-712, Las Vegas. Jan. 2011. doi: [10.1109/CCNC.2011.5766573](http://dx.doi.org/10.1109/CCNC.2011.5766573). ISBN 9781424487882. [Presentation](http://es.slideshare.net/josemariasaldana/delta-ccnc-2011presentation). [**Author's PDF version**](author_versions/delta_CCNC_2011_in_proc.pdf). IC #4.

#### Spanish Conferences

**Jose Saldana**, J. Fernández Navajas, J. Ruiz Mas, J. I. Aznar Baranda, Eduardo Viruete Navarro, L. A. Casadesus Pazos. "_Ahorro de Ancho de Banda en Juegos Online Mediante el uso de Tecnicas de Tunelado, Compresión y Multiplexion_". X Jornadas de Ingeniería Telematica (JITEL 2011), Santander. Sept. 2011. [**Author's PDF version**](author_versions/juegos_jitel_2011_in_proc.pdf). SPC #12.

**Jose Saldana**, J. Fernández Navajas, J. Ruiz Mas, J. I. Aznar Baranda, Eduardo Viruete Navarro, L. A. Casadesus Pazos. "_Influencia del Buffer del Router en la Multiplexión de Juegos Online_". Actas del XXVI Simposium Nacional de la Unión Cientifica Internacional de Radio (URSI 2011). Leganes (Spain). Sept. 2011. ISBN 9788493393458. [**Author's PDF version**](author_versions/juegos_buffer_URSI_2011_in_proc.pdf). SPC #11.

L. A. Casadesus Pazos, J. Fernández Navajas, J. Ruiz Mas, **Jose Saldana**, J. I. Aznar Baranda, Eduardo Viruete Navarro. "_Herramienta para Automatización de Medidas de Tiempo Real Extremo a Extremo_". Actas del XXVI Simposium Nacional de la Unión Científica Internacional de Radio (URSI 2011). Leganés (Spain). Sept. 2011. ISBN 9788493393458. [**Author's PDF version**](author_versions/E2E_URSI_2011_in_proc.pdf). SPC #10.

**Jose Saldana**, J. Fernández Navajas, J. Ruiz Mas, Jenifer Murillo Royo, J. I. Aznar Baranda, Eduardo Viruete Navarro, L. A. Casadesus Pazos. "_Mejora de la Calidad en un Sistema de Telefonia IP Mediante el uso de Tecnicas de Multiplexión_". X Jornadas de Ingeniería Telematica (JITEL 2011), Santander. Sept. 2011. [**Author's PDF version**](author_versions/telefonia_jitel_2011_in_proc.pdf). SPC #9.

J. I. Aznar Baranda, Eduardo Viruete Navarro, J. Ruiz Mas, J. Fernández Navajas, **Jose Saldana**. "_QMoES: Una Herramienta de Estimación de BW en Arquitecturas QoE de Banda Ancha_". X Jornadas de Ingeniería Telematica (JITEL 2011), Santander. Sept. 2011. [**Author's PDF version**](author_versions/QMoES_jitel_2011_in_proc.pdf). SPC #8.

**Jose Saldana**, J. Fernández Navajas, J. Ruiz Mas. "_Uso de la Tecnologia de Virtualización Xen en el Análisis de Protocolos de Comunicacion_". II Jornadas de Innovación Educativa en Ingeniería Telematica, en conjunción con las X Jornadas de Ingeniería Telematica (JITEL 2011), Santander. Sept. 2011. [**Author's PDF version**](author_versions/educ_jie_2011_in_proc.pdf). SPC #7.

### 2010

#### International Conferences

**Jose Saldana**, Jenifer Murillo, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo Viruete, Jose I. Aznar. "_Distributed IP Telephony System with Call Admission Control_". In Proc. Conference on ENTERprise Information Systems CENTERIS 2010. Viana do Castelo, Portugal. Oct. 2010. [**Author's PDF version**](author_versions/centeris_2010_in_proc.pdf). IC #3.

**Jose Saldana**, Eduardo Viruete, Julian Fernández-Navajas, Jose Ruiz-Mas, Jose I. Aznar. "_Hybrid Testbed for Network Scenarios_," SIMUTools 2010, the Third International Conference on Simulation Tools and Techniques. Torremolinos, Malaga (Spain). Mar. 2010. doi: [10.4108/ICST.SIMUTOOLS2010.8635](http://dx.doi.org/10.4108/ICST.SIMUTOOLS2010.8635). [**Author's PDF version**](author_versions/hybrid_SIMUTOOLS_2010_in_proc.pdf). IC #2.

#### Spanish Conferences

**Jose Saldana**, Jose Ruiz-Mas, Eduardo Viruete, Jenifer Murillo, Julian Fernández-Navajas, Jose I. Aznar. "_Testbed for Measuring Protocols and Real-Time Applications_". In Proc. I Workshop on Multimedia Data Coding and Transmission (WMDCT'2010), Valencia. Sep. 2010. [**Author's PDF version**](author_versions/TENS_CEDI_2010_in_proc.pdf). SPC #6.

Jenifer Murillo, **Jose Saldana**, Julian Fernández Navajas, Jose Ruiz Mas, Eduardo Viruete Navarro, Jose I. Aznar Baranda. "_Análisis de QoS para una Plataforma Distribuida de Telefonia IP_". Actas de las IX Jornadas de Ingeniería Telematica (JITEL 2010). Valladolid (Spain). Sep. 2010. [**Author's PDF version**](author_versions/analisis_jitel_2010_in_proc.pdf). SPC #5.

Jose Ruiz Mas, Jose I. Aznar, **Jose Saldana**, Julian Fernández-Navajas, Blanca Hernandez Ortega, Lorena Blasco Arcas, Julio Jimenez Martinez. "_Evaluación de nuevos canales de distribución en servicios interactivos IP_". IX Jornadas de Ingeniería Telematica (JITEL 2010) XX Jornadas Telecom I+D. Valladolid. Sep. 2010. [**Author's PDF version**](author_versions/IPTV_JITEL_2010_in_proc.pdf). SPC #4.

**Jose Saldana**, Jenifer Murillo, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo Viruete, Jose I. Aznar. "_Emulación de Escenarios de Red mediante un Testbed_". Actas del XXV Simposium Nacional de la Unión Cientifica Internacional de Radio (URSI 2010). Bilbao (Spain). Sep. 2010. [**Author's PDF version**](author_versions/testbed_URSI_2010_in_proc.pdf). SPC #3.

### 2009

#### International Conferences

**Jose Saldana**, Jose I. Aznar, Eduardo Viruete, Julian Fernández-Navajas, Jose Ruiz "[_QoS Measurement-Based CAC for an IP Telephony system_](https://link.springer.com/chapter/10.1007/978-3-642-10625-5_1)".QShine 2009, The Sixth International ICST Conference on Heterogeneous Networking for Quality, Reliability, Security and Robustness. Las Palmas de Gran Canaria (Spain). Nov. 2009. [Lecture Notes of the Institute for Computer Sciences, Social Informatics and Telecommunications Engineering](https://www.springer.com/series/8197), 22 Springer 2009, ISBN 978-3-642-10624-8. doi: [10.1007/978-3-642-10625-5_1](http://dx.doi.org/10.1007/978-3-642-10625-5_1). [**Author's PDF version**](author_versions/CAC_SEG_QShine_2009_in_proc.pdf). IC #1.

#### Spanish Conferences

**Jose Saldana**, Julian Fernández Navajas, Jose Ruiz Mas, Eduardo A. Viruete Navarro. "_Implementación de un CAC basado en medidas de QoS para sistemas de Telefonia IP_" .Libro de Actas de las VIII Jornadas de Ingeniería Telematica (JITEL 2009), pp. 463-466. ISBN: 978-84-96997-27-1. Cartagena (Spain). Sep. 2009. [**Author's PDF version**](author_versions/implem_jitel_2009_in_proc.pdf). SPC #2.

**Jose Saldana**, Julian Fernández-Navajas, Jose Ruiz-Mas, Eduardo A. Viruete Navarro, Luis Casadesus Pazos, Jose Ignacio Aznar Baranda. "_Sistema Multiagente de Medidas Activas E2E_" .Actas del XXIV Simposium Nacional de la Unión Cientifica Internacional de Radio (URSI 2009), pp. 247-248, ISBN 978-84-8102-550-7. Santander (Spain). Sep. 2009. [**Author's PDF version**](author_versions/MEDQoS_URSI_2009_in_proc.pdf). SPC #1.
