[Home](/README.md)

# IETF activities: RFC and drafts

[Summary of my activities at the IETF](https://datatracker.ietf.org/person/jmsaldana@fcirce.es).


## RFC 7962 (2016) Alternative Network Deployments: Taxonomy, Characterization, Technologies, and Architectures

**Jose Saldana**, Andres Arcia-Moret, Bart Braem, Ermanno Pietrosemoli, Arjuna Sathiaseelan, Marco Zennaro, [RFC7962](https://www.rfc-editor.org/info/rfc7962) "[Alternative Network Deployments: Taxonomy, Characterization, Technologies and Architectures](https://datatracker.ietf.org/doc/rfc7962/)," August 2016, [IRTF](https://irtf.org/) [GAIA (Global Access to the Internet for All)](http://www.ietf.org/mail-archive/web/gaia/current/threads.html). Contributors: Leandro Navarro, Carlos Rey-Moreno, Ioannis Komnios, Steve Song, David Lloyd Johnson, Javier Simo-Reigadas.

- [PDF version](https://www.rfc-editor.org/rfc/pdfrfc/rfc7962.txt.pdf).
- DOI: [10.17487/RFC7962](http://dx.doi.org/10.17487/RFC7962)
- [Presentation of the Alternative Networks draft](http://es.slideshare.net/josemariasaldana/alternative-network-deployments), IETF 93, Prague, Jul 2015.
- A [research paper about this RFC](http://diec.unizar.es/~jsaldana/personal/alternative_networks_COMMAG_2017.pdf).


### The IETF Journal - The Story of an RFC about Alternative Networks

**Jose Saldana**, Andres Arcia-Moret, Ioannis Komnios, "[The Story of an RFC about Alternative Networks](http://diec.unizar.es/~jsaldana/personal/story_RFC_alternative_networks_2016.pdf)," [IETF Journal](http://www.internetsociety.org/es/publications/ietf-journal),  [Nov 2016, Volume 12, Issue 2](https://www.ietfjournal.org/journal-issues/november-2016/). Also available [here](https://www.ietfjournal.org/the-story-of-an-rfc-about-alternative-networks/).


## IETF Draft - [Simplemux. A generic multiplexing protocol](http://datatracker.ietf.org/doc/draft-saldana-tsvwg-simplemux/)

Simplemux can encapsulate a number of packets belonging to different protocols into a single packet. It includes the "Protocol" field on each multiplexing header, thus allowing the inclusion of a number of packets belonging to different protocols on a packet of another protocol.

- Simplemux IETF Draft: Jose Saldana, "[Simplemux. A generic multiplexing protocol](http://datatracker.ietf.org/doc/draft-saldana-tsvwg-simplemux/)" draft-saldana-tsvwg-simplemux.

- [Presentation of Simplemux in Slideshare](http://es.slideshare.net/josemariasaldana/simplemux-traffic-optimization).

- A [research paper about Simplemux](http://diec.unizar.es/~jsaldana/personal/chicago_CIT2015_in_proc.pdf).

- [Presentation of Simplemux](http://es.slideshare.net/josemariasaldana/simplemux-a-generic-multiplexing-protocol), IETF 93, Prague, Jul 2015.

- [Implementation of Simplemux in this GitHub page](https://github.com/Simplemux/simplemux). The multiplexed bundle is sent in an IP/UDP or IP packet. It includes a [README](https://github.com/Simplemux/simplemux/blob/master/simplemux_readme.pdf) document explaining it and its installation.


## IETF Draft - [Simplemux Blast flavor](https://datatracker.ietf.org/doc/draft-saldana-tsvwg-simplemux-blast/)

Many utilities are nowadays connected via dedicated networks, but the trend toward a fully IP network is gaining more traction in some sectors (e.g. electric power).  In some use cases, although it would be desirable to avoid the use of IP networks, this may prove unavoidable.  Consequently, equipment is linked to extensive communication networks, the performance of which cannot be fully controlled or known.

Some utilities that are not connected to a dedicated network may use public wireless networks, which present certain degree of variability of some parameters (delay, jitter, packet loss, and bandwidth limits).  Considering the importance of receiving packets with a low delay, this document presents a solution for using tunnels to send frames or packets between remote facilities, with a certain degree of redundance.  This can be useful in some use cases as e.g. the sending of event-driven field commands between eletric substations.  In some cases, these messages can be very critical, and their loss or delay can make the difference between a blackout and a simple outage.

Considering the high redundancy of the protocol, its use must be restricted to traffic flows which require very low delay to control critical equipment.


## IETF Draft - Delay Limits for Real-Time Services

Mirko Suznjevic, Jose Saldana, "[_Delay Limits for Real-Time Services_](https://datatracker.ietf.org/doc/draft-suznjevic-tsvwg-delay-limits/)". 2016.


## IETF Draft - Header compression and multiplexing in LISP

"[Header compression and multiplexing in LISP](https://datatracker.ietf.org/doc/draft-saldana-lisp-compress-mux/)," submitted to the IETF LISP WG.

- [Presentation of the draft in Slideshare](http://es.slideshare.net/josemariasaldana/header-compression-and-multiplexing-in-lisp).

## The IETF Journal - It's Time to Give Online Games Serious Consideration

Jose Saldana, Mirko Suznjevic, "[It's Time to Give Online Games Serious Consideration](https://www.ietfjournal.org/its-time-to-give-online-games-serious-consideration/)", [IETF Journal](https://www.ietfjournal.org/), [Nov 2013, Volume 9, Issue 2](https://www.ietfjournal.org/journal-issues/november-2013/).

## The IETF Journal - Shooting Around the Corner, The problem of Real-Time Services

Jose Saldana, Dan Wing, Julian Fernandez-Navajas, Jose Ruiz-Mas, Muthu A.M. Perumal, Gonzalo Camarillo, Michael Ramalho, "[Shooting Around the Corner, The problem of Real-Time Services](https://www.ietfjournal.org/shooting-around-the-corner-the-problem-of-real-time-services/)", [IETF Journal](https://www.ietfjournal.org/), [June 2012, Volume 8, Issue 1](https://www.ietfjournal.org/journal-issues/june-2012/). [Newcomer Experience: Arranging a Last-Minute Online Gaming Tutorial](https://www.ietfjournal.org/shooting-around-the-corner-the-problem-of-real-time-services/).

## “Online games tutorial,” Transport Area Open Meeting, Aug 2013, IETF87, Berlin, Germany.

https://gitlab.com/josemariasaldana/research/-/blob/main/talks.md#online-games-tutorial-transport-area-open-meeting-aug-2013-ietf87-berlin-germany
