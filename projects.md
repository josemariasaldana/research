[Home](/README.md)

# R&D projects funded through competitive calls of public entities

## European level

[Horizon Europe eFort](https://efort-project.eu/) ([G.A. No 101075665](https://doi.org/10.3030/101075665)). 1 September 2022 to 31 August 2026. Role: researcher.

[H2020 TIGON](https://tigon-project.eu/) ([G.A. No 957769](https://cordis.europa.eu/project/id/957769/en)). 1 September 2020 to 31 August 2024. Role: researcher.

[H2020 CORALIS](https://www.coralis-h2020.eu/) ([G.A. No 958337](https://cordis.europa.eu/project/id/958337)). 1 October 2020 to 30 September 2024. Role: researcher in WP3 and 4.

[H2020 FARCROSS](https://farcross.eu/), FAcilitating Regional CROSS-border Electricity Transmission through Innovation, ([G.A. No 864274](https://cordis.europa.eu/project/id/864274)). 1 October 2019 to 30 September 2023. Role: researcher in WP6.

[H2020 Wi-5 project](https://github.com/Wi5) ([G.A. No 644262](https://cordis.europa.eu/project/id/644262)). 1 January 2015 to 30 April 2018. Role: leader of WP3.

[RUBENS project](https://www.celticnext.eu/project-rubens/) (CELTIC 2008-2010, CP5-020). Role: researcher.

## National level

Cuantificación de la calidad percibida por el usuario (QoE) para flujos IP integrados en servicios de banda ancha, MINISTERIO DE CIENCIA E INNOVACION, TIN2010-17298, 2011-2013

RTCTCM, Red Temática en Codificación y Transmisión de Contenidos Multimedia, MINISTERIO DE CIENCIA E INNOVACION, TEC2010-11776-E, 2011-2012, 1 year.

## Regional level (Aragon, Spain)

[Tecnologías Audio-visuales Multimodales Avanzadas (TAMA)](https://tama.unizar.es/el-proyecto-tama/), 2013-2014. Funded by Government of Aragon. Role: Technical Coordinator.

Implementación de un MBAC basado en QoS para Sistemas ToIP, Aragon I+D, 2010-2011.


# Contracts with private companies

[Wireless LAN based on use of Light Virtual WiFi Access Points](https://i3a.unizar.es/en/projects/wireless-lan-based-use-light-virtual-wifi-access-points), financed by Orange Labs, France. Role: Principal Investigator. Code J02181. 2019-2020. Duration: 1 year.

Análisis del tráfico generado por la plataforma de videoconferencia VIDYO de alta definición, financed by Orbe Telecomunicaciones S.L.. Role: Researcher. 2011.

Cátedra Telefónica 2010. Nuevos Canales de Distribución en Servicios Interactivos IP: Cuantificación de la Calidad de la Experiencia. Role: Researcher. 2010.
