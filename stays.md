[Home](/README.md)

# Research stays in public or private R&D centres 

Jan – Apr 2017: Research Scholarship, TNO, The Hague, The Netherlands.

May – Jul 2012: Research Scholarship, Department Scienze dell'informazione, Università di Bologna, Italy.
