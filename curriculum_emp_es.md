[Home](/README.md)

# Curriculum vitae
**Actualizado 22/3/2023**

[Perfil en LinkedIn](https://www.linkedin.com/in/josesaldanamedina/?locale=en_US)

## Objetivo Profesional

Mis líneas de investigación principales están relacionadas con:
- redes programables, cableadas e inalámbricas (principalmente WiFi en entornos domésticos e industriales)
- digitalización de redes industriales
- estudio de servicios de tiempo real, como voz sobre IP y videojuegos online
- estimación de la calidad objetiva y subjetiva en servicios de tiempo real
- ciberseguridad en entornos WiFi e IoT
- optimización y compresión de tráfico de voz y juegos


## Experiencia

### CIRCE Centro Tecnológico (dic 2020 - presente)
*Investigador*

Trabajo en proyectos privados y públicos (Horizonte 2020 y Horizonte Europa) en los siguientes ámbitos:
- IoT en entornos industriales: comunicaciones inalámbricas.
- Digitalización de subestaciones.
- Ciberseguridad en subestaciones y otras infraestructuras críticas.


### Universidad de Zaragoza (2008 - nov 2020)
*Doctor Investigador Senior*

Comencé en septiembre de 2008 haciendo el Doctorado, que terminé en diciembre de 2011.


## Proyectos

He trabajado en diferentes proyectos de investigación, financiados tanto con fondos públicos como por empresas. Se pueden destacar estos:
- Actualmente trabajo en H2020 [FARCROSS](https://farcross.eu/), H2020 [CORALIS](https://www.coralis-h2020.eu/) y Horizonte Europa [eFort](https://efort-project.eu/).
- Entre 2015 y 2018 trabajé en el proyecto H2020 [Wi5 (What to do With the Wi-Fi Wild West)](https://cordis.europa.eu/project/id/644262). En ese proyecto fui líder de uno de los cinco Paquetes de Tabajo. Los socios fueron Liverpool John Moores University (Reino Unido), TNO (Holanda), Telefónica y AirTies (Turquía), además de la Universidad de Zaragoza.
- Durante un año fui Investigador Principal de un proyecto financiado por Orange Labs (Francia) sobre Coordinación de entornos Wi-Fi domésticos mediante puntos de acceso virtuales: [Wireless LAN based on use of Light Virtual WiFi Access Points](https://i3a.unizar.es/en/projects/wireless-lan-based-use-light-virtual-wifi-access-points).
- He participado en la escritura de varios proyectos FP7 y H2020.

## Investigación

He publicado 21 artículos en revistas científicas internacionales, y he participado en la publicación de 35 artículos en congresos internacionales, y 16 nacionales. He colaborado con más de 30 coautores para estos artículos.

Desde 2017 soy miembro del Consejo Editorial de la revista científica IEEE Access. También he realizado revisiones de 30 artículos para revistas, y he sido invitado al comité científico de 36 congresos internacionales, participando en el comité de organización de 10 de ellos. Desde 2010 soy miembro del IEEE.

## Estandarización

Tengo experiencia de estandarización en el IETF (Internet Engineering Task Force). Fui el coordinador del documento [RFC 7962 “Alternative Network Deployments: Taxonomy, Characterization, Technologies and Architectures](http://dx.doi.org/10.17487/RFC7962),” en el GAIA (Global Access to the Internet for All) group. 

## Docencia y supervisión de estudiantes

He codirigido la tesis doctoral “[IoTsafe: Método y sistema de comunicaciones seguras para ecosistemas IoT](https://zaguan.unizar.es/record/94492?ln=en)” (Autor Jorge David de Hoz Diego, julio 2020). He dirigido 6 Proyectos Fin de Carrera, 5 Trabajos Fin de Grado y un Trabajo Fin de Máster.

## Sesiones invitadas y tutoriales

- Sesión invitada en la Universidad de Zagreb, invitado por la Sección Croata del IEEE Communications Society, 2012.
- Sesión invitada en el Workshop NIME del congreso IEEE CCNC, enero 2014, Las Vegas.
- Sesión invitada en la Liverpool John Moores University en 2015.  


## Formación

- Doctorado “Tecnologías de la información y comunicaciones en redes móviles”, Universidad de Zaragoza (2008-2011)
- Master Universitario en Tecnologías de la Información y Comunicaciones en Redes Móviles, Universidad de Zaragoza (2006-2008)
- Ingeniero de Telecomunicación Especialidad Telemática, Universidad de Zaragoza, (1992-1998)


## Otras habilidades

### Idiomas

Inglés: 7º curso del Centro de Lenguas Modernas de la Universidad de Zaragoza, 2019 (equivalente a C1).

### Programación y otros

Conocimiento avanzado de Linux. Compilación para sistemas embebidos (p.ej. router Wi-Fi con OpenWrt).
He programado en C, C++, Matlab y también tengo ciertos conocimientos de Java.

### [Página en Google Scholar](https://scholar.google.com/citations?hl=es&user=O3aa4uoAAAAJ)

- Citas 671	
- Índice h 15	
- Índice i10 23

He participado en varias actividades de divulgación científica, como el festival [Pint of Science](https://pintofscience.es/) o la Noche de los Investigadores.

[Currículum académico detallado](https://cvn.fecyt.es/0000-0002-6977-6363).

Página de GitHub: https://github.com/josemariasaldana 
