[Home](/README.md)

# General quality indicators of scientific research

## [Google Scholar, user O3aa4uoAAAAJ](http://scholar.google.com/citations?hl=es&amp;user=O3aa4uoAAAAJ)

Updated 24/9/2024

- Total articles: 110
- Total cites: 776
- h index: 15
- i10 index: 28

## [Scopus, author Id 37041446900](http://www.scopus.com/authid/detail.url?authorId=37041446900)

Updated 24/9/2024

- Total Articles: 67
- Total cites: 380
- h index: 10     

## [Web of Science, researcher ID K-6595-2014](http://www.webofscience.com/wos/author/record/K-6595-2014)

Updated 24/9/2024

### Profile summary

- 74 Total documents 
- 62 Publications indexed in Web of Science
- 53 Web of Science Core Collection publications
- 9 Preprints
- 0 Dissertations or Theses
- 12 Non-indexed publications
- 52 Verified peer reviews
- 13 Verified editor records


### Web of Science Core Collection metrics
- H-Index 8
- Publications in Web of Science 53
- Sum of Times Cited 233
- Citing Articles 187
- Sum of Times Cited by Patents 2
- Citing Patents 2


## [DBLP, Computer Science Bibliography](https://dblp.uni-trier.de/pers/hd/s/Saldana:Jose)

- 64 publications (22 journals, 30 conferences, 1 editorship, 1 other publications)    


# [Journal Citations Reports](https://clarivate.com/products/scientific-and-academic-research/research-analytics-evaluation-and-management-solutions/journal-citation-reports/): 25 articles

(13 articles as first author).

## Total publications first quartile (Q1): 7
- [IEEE Communications Magazine](https://www.comsoc.org/publications/magazines/ieee-communications-magazine) (4 articles)
- [IEEE Access](https://ieeeaccess.ieee.org/) (2 articles)
- [IEEE Internet of Things Journal](https://ieee-iotj.org/) (1 article)

## Total publications second quartile (Q2): 13
- [IEEE Power & Energy Magazine](https://magazine.ieee-pes.org/) (1 article)
- [IEEE Transactions on Power Delivery](https://ieee-pes.org/publications/transactions-on-power-delivery/) (1 article)
- [Computer Networks](https://www.sciencedirect.com/journal/computer-networks) (1 article)
- [IEEE Communications Letters](https://www.comsoc.org/publications/journals/ieee-comml) (4 articles)
- [Multimedia Tools and Applications](https://www.springer.com/journal/11042) (2 articles)
- [IEEE Access](https://ieeeaccess.ieee.org/) (2 articles) 
- [Sensors](https://www.mdpi.com/journal/sensors) (2 articles) 

## Total publications third quartile (Q3): 3
- [Data in brief](https://www.sciencedirect.com/journal/data-in-brief) (1 article)
- [Energies](https://www.mdpi.com/journal/energies) (1 article)
- [KSII Transactions on Internet and Information Systems](https://www.itiis.org/) (1 article)

## Total publications fourth quartile (Q4): 2
- [KSII Transactions on Internet and Information Systems](https://www.itiis.org/) (1 article)
- [IEEE Latin America Transactions](https://latamt.ieeer9.org/index.php/transactions) (1 article)


# PhD Supervision

I have supervised a PhD thesis titled “[IoTsafe: Método y sistema de comunicaciones seguras para ecosistemas IoT](https://zaguan.unizar.es/record/94492)” (Jorge David de Hoz Diego).
