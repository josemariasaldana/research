# Jose Saldana, Project Manager at [CIRCE - Technology Center](https://www.fcirce.es/en/)

[Publications](https://gitlab.com/josemariasaldana/research/-/blob/main/publications.md) -
[Patents](https://gitlab.com/josemariasaldana/research/-/blob/main/patents.md) -
[Research Projects](https://gitlab.com/josemariasaldana/research/-/blob/main/projects.md) -
[IETF activities and RFC](https://gitlab.com/josemariasaldana/research/-/blob/main/RFC.md) -
[Editorial Boards & Reviews](https://gitlab.com/josemariasaldana/research/-/blob/main/boards.md) -
[Conference Committees](https://gitlab.com/josemariasaldana/research/-/blob/main/committees.md) -
[Supervised Works](https://gitlab.com/josemariasaldana/research/-/blob/main/supervision.md) -
[Invited talks](https://gitlab.com/josemariasaldana/research/-/blob/main/talks.md) -
[Research stays](https://gitlab.com/josemariasaldana/research/-/blob/main/stays.md)

## Profiles
[Google Scholar](http://scholar.google.com/citations?hl=es&amp;user=O3aa4uoAAAAJ) -
[LinkedIn](http://www.linkedin.com/in/josesaldanamedina/en) -
[Orcid 0000-0002-6977-6363](http://orcid.org/0000-0002-6977-6363) -
[Scopus author Id 37041446900](http://www.scopus.com/authid/detail.url?authorId=37041446900) -
[Researchgate](https://www.researchgate.net/profile/Jose_Saldana) -
[Web of Science Researcher ID K-6595-2014](http://www.webofscience.com/wos/author/record/K-6595-2014) -
[dblp](https://dblp.uni-trier.de/pers/hd/s/Saldana:Jose) -
[IEEEXplore](https://ieeexplore.ieee.org/author/38275641300) -
[ACM profile](https://dl.acm.org/profile/81464651833) -
[SciProfiles](https://sciprofiles.com/profile/josesaldana) -
[Slideshare](http://es.slideshare.net/josemariasaldana) -
[Github](https://github.com/josemariasaldana) -
[Experiences in Gitlab](https://gitlab.com/josemariasaldana/experiences) -
[My normalized Curriculum Vitae](https://cvn.fecyt.es/0000-0002-6977-6363) (Spanish Ministry).

## Short bio

Fifteen years of research experience, in the fields of wired and wireless networks with tight delay constraints, including multimedia services and digital communications in electric substations. After twelve years in academia, I moved to [CIRCE Technology Center](https://www.fcirce.es/en/), where my main role is the participation in research projects related with the digitalisation of power systems, including ICT performance and security, and the use of wireless communications in industrial environments.

[Senior Member](https://cis.ieee.org/activities/membership-activities/ieee-member-directory), [IEEE, Institute of Electrical and Electronics Engineers](https://www.ieee.org/), [Communications Society](https://www.comsoc.org/).

## Education

- PhD in Information Technologies 2011, [Universidad de Zaragoza](https://www.unizar.es), Spain. ["Técnicas de Optimización de Parámetros de Red para la mejora de la Comunicación en Servicios de Tiempo Real" (Techniques for the Optimization of Network Parameters for Communication Improvement in Real-Time Services)](https://dialnet.unirioja.es/servlet/tesis?codigo=204881). Dec. 2011. [PDF version](author_versions/doctoral_thesis_jsaldana.pdf)
- Master's Degree of Information Technology and Communications in Mobile Networks 2008.
- Telecommunications Engineer 1998.
- Postgraduate qualification: [Experto Universitario en Gestión del I+D+i en la Empresa](https://escueladoctorado.unizar.es/es/noticia/titulo-propio-experto-universitario-en-gestion-del-idi-en-la-empresa) (Título propio de la Univ. Zaragoza) Dec 2020.

## Curriculum

- [CV in Spanish](curriculum_emp_es.md).
- [Summary of the CV, in Spanish](curriculum_summary_es.md).
- [Summary of the CV, in English](curriculum_summary_eng.md).

## General quality indicators of scientific research

- [General quality indicators of scientific research, in English](quality_indicators.md)
