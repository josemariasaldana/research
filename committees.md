[Home](/README.md)

# Conference Committees

I have served on the steering committee and TPC for many prestigious conferences as IEEE Consumer Communications and Networking, ACM Multimedia Systems Conference, IEEE ICC, IEEE Globecom, etc:

- Member of the Technical Program Committee, [Global Information Infrastructure and Networking Symposium, GIIS'24](https://www.giis2023.org/home), February 19-21, 2024 - Dubai - UAE. Organized by [IEEE ComSoc](https://www.comsoc.org/conferences-events/clonglobal-information-infrastructure-and-networking-symposium-2024).

- Member of the Technical Program Committee, [ACM Mobicom 2023, The 29th Annual International Conference On Mobile Computing And Networking](https://sigmobile.org/mobicom/2023/), 2-6 Oct 2023, Madrid, Spain. [Poster/Student Research Competition (SRC)](https://sigmobile.org/mobicom/2023/posters_cfp.html) track.

- Member of the Technical Program Committee, [IEEE Globecom 2023, Global Communications Conference](https://globecom2023.ieee-globecom.org/), NGNI (Next-Generation Networking and Internet). 4/8 December 2023 // Kuala Lumpur, Malaysia.

- Member of the Technical Program Committee, [IEEE Globecom 2022, Global Communications Conference](https://globecom2022.ieee-globecom.org/), NGNI. 4-8 December 2022 // Rio de Janeiro, Brazil

- Member of the Technical Program Committee, [IEEE Globecom 2020, Global Communications Conference](https://globecom2020.ieee-globecom.org/), SAC IoTSCC, NGNI. 7-11 December 2020 // Taipei, Taiwan

- Member of the Technical Program Committee, [IEEE 5G World Forum 2020](https://ieee-wf-5g.org/), 10-12 Sep 2020, virtual event

- Member of the Technical Program Committee, [IEEE International Conference on Communications, ICC'20](https://icc2020.ieee-icc.org/) - [NGNI Symposium](https://icc2020.ieee-icc.org/sites/icc2020.ieee-icc.org/files/NGNI.pdf), 7-11 June 2020, Dublin, Ireland.

- Member of the Technical Program Committee, [Netproc 2020 Workshop (1st Workshop on Flexible Network Data Plane Processing)](https://www.icin-conference.org/2020/netproc-2020/), at [ICIN 2020, 23rd Conference on Innovation in Clouds, Internet and Networks](https://www.icin-conference.org/2020/), Feb. 2020, Paris, France.

- Member of the Technical Program Committee, [IEEE 5G World Forum 2019, 30 Sep-2 Oct 2019](http://ieee-wf-5g.org/), Dresden, Germany

- Member of the Technical Program Committee, [IEEE Global Communications Conference Globecom 2019. Selected Areas in Communications: Internet of Things](http://globecom2019.ieee-globecom.org/), 9-13 December 2019, Waikoloa, HI, USA.

- Member of the Technical Program Committee, [24th IEEE International Symposium on Computer and Communications (ISCC 2019)](http://paradise.site.uottawa.ca/iscc2019/), June 29th - July 2nd, 2019, Barcelona, Spain.

- Member of the Technical Program Committee, [ACM Multimedia Systems Conference (MMSys 2019)](http://www.mmsys2019.org/), [Demo Track](http://www.mmsys2019.org/participation/demo-cfp/), June 18 - 21, 2019, Amherst, MA, USA.

- Member of the Technical Program Committee, [IEEE WCNC 2019, IEEE Wireless Communications and Networking Conference](http://wcnc2019.ieee-wcnc.org/), 15-18 April 2019, Marrakech, Morocco.

- Member of the Technical Program Committee, [IEEE ICC'19 - NGNI Symposium](http://icc2019.ieee-icc.org/), 20-24 May 2019, Shanghai, China.

- Member of the Technical Program Committee, [IEEE Global Communications Conference](http://globecom2018.ieee-globecom.org/): [Selected Areas in Communications: Internet of Things (Globecom2018 SAC IoT)](http://globecom2018.ieee-globecom.org/sites/globecom2018.ieee-globecom.org/files/symposia/SAC-Internet%20of%20Things.pdf). and [Next-Generation Networking](http://globecom2018.ieee-globecom.org/sites/globecom2018.ieee-globecom.org/files/symposia/Next-Generation%20Networking%20Symposium.pdf) Symposium. 9-13 December 2018, Abu Dhabi, UAE.

- Member of the Technical Program Committee, [MSWIM2018, The 21st ACM International Conference on Modeling, Analysis and Simulation of Wireless and Mobile Systems](http://mswimconf.com/2018/), October 28th - November 2nd, 2018, Montreal, Canada.

- Member of the Technical Program Committee, [IEEE 5G World Forum 2018 (WF-5G)](http://ieee-wf-5g.org/), 9-11 July 2018, Santa Clara, California, USA.

- Member of the Technical Program Committee, [23rd IEEE International Symposium on Computer and Communications (ISCC 2018)](http://iscc2018.ieee-iscc.org/), June 25-28, 2018, Natal, Brazil

- Member of the Technical Program Committee, [ACM Multimedia Systems Conference (MMSys)](http://mmsys2018.org/), Demo Track. 12-15 June 2018, Amsterdam, The Netherlands.

- Member of the Technical Program Committee, [NetGames 2018 workshop](http://conferences.telecom-bretagne.eu/netgames18/), co-located with [ACM Multimedia Systems Conference (MMSys)](http://mmsys2018.org/). 12-15 June 2018, Amsterdam, The Netherlands.

- Member of the Technical Program Committee, [IEEE ICC 2018, Next Generation Networking and Internet Symposium](http://icc2018.ieee-icc.org/content/welcome-ieee-icc-2018), 20-24 May 2018, Kansas City, MO, USA.

- Member of the Technical Program Committee, [IEEE/IFIP Network Operations and Management Symposium (NOMS)](http://noms2018.ieee-noms.org/), 23-27 April 2018, Taipei, Taiwan, [Demo track](http://noms2018.ieee-noms.org/content/call-demos).

- Member of the Technical Program Committee, [IEEE Globecom 2017](http://globecom2017.ieee-globecom.org/), [Next-Generation Networking and Internet Symposium](http://globecom2017.ieee-globecom.org/sites/globecom2017.ieee-globecom.org/files/u58/CFP%20NGNI%20GC%202017%20v2.pdf), 4-8 December 2017, Singapore.

- Member of the Technical Program Committee, [MSWIM 2017 ACM/IEEE International Conference on Modelling, Analysis and Simulation of Wireless and Mobile Systems](http://mswimconf.com/2017/), 21-25 Nov 2017, Miami, USA.

- Member of the Technical Program Committee, [IEEE ISCC 2017, The 22nd IEEE Symposium on Computers and Communications](http://www.ics.forth.gr/iscc2017/index.html), 03 - 06 July 2017, Heraklion, Crete, Greece

- Publications Chair of [SPECTS 2017, International Symposium on Performance Evaluation of Computer and Telecommunication Systems](http://atc.udg.edu/SPECTS2017/), SCS, July 9-12, 2017, Seattle, WA, USA.

- Member of the Technical Program Committee, [NetGames 2017, The 15th Annual Workshop on Network and Systems Support for Games](http://mmsys17.iis.sinica.edu.tw/netgames/), co-located within the [ACM Multimedia Systems 2017](http://mmsys17.iis.sinica.edu.tw/), Taipei, Taiwan | June 20-23, 2017.

- Member of the Technical Program Committee, [IEEE International Conference on Communications, ICC 2017](http://icc2017.ieee-icc.org/), [Next Generation Networking and Internet Symposium](http://icc2017.ieee-icc.org/sites/icc2017.ieee-icc.org/files/u51/ICC%202017%20CfP%20NGNI.pdf); [Selected Areas in Communications: Cloud Communications and Networking](http://icc2017.ieee-icc.org/sites/icc2017.ieee-icc.org/files/u51/ICC%202017%20CfP%20SAC-3%20CCN.pdf); 21-25 May 2017, Paris, France.

- Publications Chair of [IEEE CCNC 2017, The 14th annual IEEE Consumer Communications & Networking Conference](http://ccnc2017.ieee-ccnc.org/), Jan 8-11, 2017, Las Vegas, Nevada, USA.

- Member of the Technical Program Committee, [IEEE CCNC 2017, Sensing, Smart Spaces and IoT: Applications and QoE track](http://ccnc2017.ieee-ccnc.org/), The 14th annual IEEE Consumer Communications & Networking Conference, Jan 8-11, 2017, Las Vegas, Nevada, USA.  

- Member of the Technical Program Committee, [IEEE Globecom 2016](http://globecom2016.ieee-globecom.org/), [Next Generation Networking Symposium (NGNI)](http://globecom2016.ieee-globecom.org/sites/globecom2016.ieee-globecom.org/files/u46/CFP%20Nex%20Gen%20Network%20GC%202016-v4.pdf), [Selected Areas in Communications - Cloud Networks](http://globecom2016.ieee-globecom.org/sites/globecom2016.ieee-globecom.org/files/u46/CFP_SAC-Cloud-Networks_GC_2016_v4.pdf), San Diego, CA, Dec. 2016.

- Member of the Technical Program Committee, [IEEE ICC 2016, Next Generation Networking and Internet Symposium](http://icc2016.ieee-icc.org/), 23-27 May 2016, Kuala Lumpur, Malaysia.

- Publications Chair of [IEEE CCNC 2016, The 13th annual IEEE Consumer Communications & Networking Conference](http://ccnc2016.ieee-ccnc.org/), Jan 9-12, 2016, Las Vegas, Nevada, USA.

- Publications Chair of [IEEE 2nd World Forum on Internet of Things (WF-IoT) - Enabling Internet Evolution](http://www.ieee-wf-iot.org/), Dec 14-16, 2015, Milan, Italy.

- Member of the Technical Program Committee, [IEEE Globecom 2015](http://globecom2015.ieee-globecom.org/), [Next Generation Networking Symposium (NGNI)](http://globecom2015.ieee-globecom.org/sites/globecom2015.ieee-globecom.org/files/u42/GC15_TPC_CFP_NGN_-_Next-Generation_Networking.pdf), [Selected Areas in Communications - Internet of Things](http://globecom2015.ieee-globecom.org/sites/globecom2015.ieee-globecom.org/files/u42/GC15_TPC_CFP_SAC_-_Internet_of_Things.pdf), San Diego, CA, Dec. 2015.

- Member of the Technical Program Committee, [ICCC 2015, IEEE/CIC International Conference on Communications in China](http://www.ieee-iccc.org/), 2-4 November 2015, Shenzhen, China

- Member of the Technical Program Committee, [Netgames 2015, The 14th International Workshop on Network and Systems Support for Games](http://netgames2015.fer.hr/), Zagreb, Croatia, December 3-4, 2015. In co-operation with ACM SIGCOMM and ACM SIGMM.

- Member of the Technical Program Committee, [IEEE ICCVE 2015, The 4th International Conference on Connected Vehicles & Expo](http://www.iccve.org/2014/), Shenzen, China, Oct 19-23, 2015.

- Member of the Technical Program Committee, [Special Session [Service and Infrastructure Management for Cloud, Virtualized and Next Generation Networks,](http://www.ieee-camad.org/special_session/simc.pdf)  IEEE CAMAD 2015, University of Surrey, Guildford, UK, 7-9 September 2015.

- Publications Chair of [SPECTS 2015, International Symposium on Performance Evaluation of Computer and Telecommunication Systems](http://atc.udg.edu/SPECTS2015/), SCS, July 26-29, 2015, Chicago, IL, USA .

- Member of the Technical Program Committee, [IEEE ICC 2015](http://icc2015.ieee-icc.org/), Next Generation Networking and Internet Symposium, London, 8-12 June 2015.

- Publications Chair of [IEEE CCNC 2015, The 12th annual IEEE Consumer Communications & Networking Conference](http://ccnc2015.ieee-ccnc.org/), Jan 9-12, 2015, Las Vegas, Nevada, USA.

- Member of the Technical Program Committee, [IEEE CCNC 2015, the 12th annual IEEE Consumer Communications & Networking Conference](http://ccnc2015.ieee-ccnc.org/), Networked Games and Social, P2P and Multimedia Networking, Service and Applications tracks, Jan 9-12, 2015, Las Vegas, Nevada, USA.

- Member of the Technical Program Committee, [CECNet 2014, the 4th International Conference on Consumer Electronics, Communications and Networks](http://www.cecnetconf.org/index.html), Beijing, China, 20-22 Dec. 2014.

- Member of the Technical Program Committee, [IEEE Globecom 2014](http://www.ieee-globecom.org/), [Next Generation Networking Symposium (NGNI)](http://www.ieee-globecom.org/2014/GC2014-NGN-CFP-V4.pdf), Dec. 2014.

- Member of the Technical Program Committee, [IEEE ICCVE 2014, The 3rd International Conference on Connected Vehicles & Expo](http://www.iccve.org/2014/), Vienna, Austria, Nov 2-7, 2014.

- Member of the Technical Program Committee, [SC2 2014](http://grid.chu.edu.tw/sc22014/), The Fourth International Symposium on Cloud and Service Computing, Beijing, China, Sept. 1-2, 2014.

- Member of the Technical Program Committee, [Chinacom 2014](http://chinacom.org/2014/show/home), [European Alliance of Innovation](http://eai.eu/), Maoming, People's Republic of China, Aug 2014.

- Member of the Technical Program Committee, [International Conference on Internet of Vehicles (IOV 2014)](http://www.bjiov.org/), Beijing, Aug. 2014.

- Member of the Technical Program Committee, [QShine 2014](http://qshine.org/2014/show/home), [European Alliance of Innovation](http://eai.eu/), Rhodes, Greece, Aug 2014.

- Member of the Program Committee, [I Workshop pre-IETF](http://workshop.protocolos.net.br/enindex.php), 30 July 2014, Brasilia DF Brazil, Side Event to CSBC 2014 (XXXIV Congress of SBC)

- Publications Chair of [SPECTS 2014 conference, International Symposium on Performance Evaluation of Computer and Telecommunication Systems](http://atc.udg.edu/SPECTS2014/), SCS, July 6-10, 2014, Monterey, CA, USA.

- Member of the Technical Program Committee, [IEEE International Workshop on Cloud Gaming Systems and Networks (C-Game)](https://sites.google.com/site/icmecgames2014/), held in conjunction with [IEEE International Conference on Multimedia and Expo (ICME) 2014](http://www.icme2014.org/), Chengdu, China. July 2014.

- Co-Chair of the [Demonstrations Track](http://ccnc2014.ieee-ccnc.org/call-for-submissions/call-for-demonstration) of [IEEE CCNC 2014, The 11th annual IEEE Consumer Communications & Networking Conference](http://ccnc2014.ieee-ccnc.org/), Jan 10-13, 2014, Las Vegas, Nevada, USA.

- Member of the Technical Program Committee, [IEEE CCNC 2014, Networked Games track, The 11th annual IEEE Consumer Communications & Networking Conference](http://ccnc2014.ieee-ccnc.org/), Jan 10-13, 2014, Las Vegas, Nevada, USA.

- Member of the Technical Program Committee, [IEEE CCNC 2014, Work in Progress track, The 11th annual IEEE Consumer Communications & Networking Conference](http://ccnc2014.ieee-ccnc.org/), Jan 10-13, 2014, Las Vegas, Nevada, USA.

- Member of the Technical Program Committee, [ICCVE 2013, 2013 International Conference on Connected Vehicles & Expo](http://www.iccve.org/), Las Vegas, 2-6 December 2013.

- Member of the Technical Program Committee, [Chinacom 2013 conference](http://chinacom.org/2013/), Network and Information Security track.

- Member of the Technical Program Committee, [IEEE Globecom 2013](http://www.ieee-globecom.org/2013/), [Next Generation Networking and Internet Symposium (NGNI)](http://www.ieee-globecom.org/2013/CFP-GC13-NGN.pdf).

- Reviewer of [IEEE CCNC 2013, Multimedia Networking & Services & Applications](http://www.ieee-ccnc.org/2013/).

- Publications Chair of [SPECTS 2013, International Symposium on Performance Evaluation of Computer and Telecommunication Systems](http://atc.udg.edu/SPECTS2013/), SCS.

- Member of the Technical Program Committee in [ACM NOSSDAV 2012](http://london.csl.toronto.edu/nossdav12/index.html) and [NOSSDAV 2013](http://nossdav2013.ndlab.net/) conferences.

- Member of the Technical Program Committee in [NIME 2012 Workshop](http://www.math.unipd.it/~cpalazzi/NIME12/), Co-Located with [IEEE ICCCN 2012](http://www.icccn.org/icccn12/) conference.

- Member of the Technical Program Committee in the [4th IEEE International Workshop on Digital Entertainment, Networked Virtual Environments, and Creative Technology](http://cms.comsoc.org/eprise/main/SiteGen/CCNC_2012/Content/Home/workshops/DENVECT.html), held in conjunction with [IEEE CCNC 2012](http://ccnc2012.ieee-ccnc.org/).

- Reviewer and session chair of [IFIP NTMS 2011](http://www.ntms-conf.org/) conference.

- Member of the Organizing and Technical Program Committees of ]ICST QShine 2010](http://qshine.org/) conference.
