[Home](/README.md)

# Invited talks

[José Mª Saldaña](https://orcid.org/0000-0002-6977-6363) y [Miguel Ángel Oliván](https://orcid.org/0000-0001-7025-061X), [IEEE PES, sección España](https://ieeespain.org/tag/pes/), [Webinar 02.05.2024](https://events.vtools.ieee.org/m/416492): "[_Retos y desarrollos en la digitalización de las subestaciones: el bus de Proceso_](presentations/webinar_CIRCE_IEEE_PES_2024.05.02.pdf)".

“[_Online games: a real-time problem for the network_](http://es.slideshare.net/josemariasaldana/online-games-a-realtime-problem-for-the-network),” Department of Computer Science, Liverpool John Moores University, UK, Oct. 2015.

NIME Workshop Invited Talk: “[_The problem of using a best-effort network for online games_](http://es.slideshare.net/josemariasaldana/nime-v6),” [10th IEEE International Workshop on Networking Issues in Multimedia Entertainment NIME'14](http://ccnc2014.ieee-ccnc.org/sites/ccnc2014.ieee-ccnc.org/files/u18/IEEENIME2014-cfp.pdf), held in conjunction with [Consumer Communications and Networking Conference, CCNC 2014](http://ccnc2014.ieee-ccnc.org/). Las Vegas, Nevada, USA, January 10, 2014.

Tutorial “[_Online Games: Traffic Characterization and Network Support_](http://es.slideshare.net/josemariasaldana/online-games-traffic-characterization-and-network-support),” [IEEE Consumer Communications and Networking Conference, CCNC 2014](http://ccnc2014.ieee-ccnc.org/), Las Vegas, NV, USA.


“Online games: is the Internet prepared for them?” IEEE Communications Society, Croatian Section. FER (Fakultet Elektrotehnikei Računarstva), University of Zagreb, Croatia, May 2012.


# “Online games tutorial,” Transport Area Open Meeting, Aug 2013, IETF87, Berlin, Germany.

[Slides of the session](http://www.ietf.org/proceedings/87/slides/slides-87-tsvarea-1.pdf)

[Video stream of the session](http://new.livestream.com/internetsociety/onlinegames)

## Promotional videos presenting the session

[![First promotional video](https://img.youtube.com/vi/_G96ULX7Iak/0.jpg)](https://www.youtube.com/watch?v=_G96ULX7Iak)

[![Matrix promotional video](https://img.youtube.com/vi/QAnW9HTkbsI/0.jpg)](https://www.youtube.com/watch?v=QAnW9HTkbsI)
