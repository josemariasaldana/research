[Home](/README.md)

# Student supervision

## PhD Thesis
Supervisor of a Doctoral Thesis “[IoTsafe: Método y sistema de comunicaciones seguras para ecosistemas IoT](https://zaguan.unizar.es/record/94492?ln=en)” (Author Jorge David de Hoz Diego). July 2020.

## Final Year Projects

TFG: [Medidas digitales en Smart Grids: Generación de datos, Transporte y Evaluación del rendimiento en redes WAN. Digital Measurements in Smart Grids: Data Generation, Traffic Transport, and Performance Evaluation in WANs]() / Manuel Gutiérrez Lardiés / University of Zaragoza, 2024.

TFG: [Gestión de la distribución del tráfico en un entorno WLAN](https://zaguan.unizar.es/record/77507?ln=es) / Pedro Forton Rubio / University of Zaragoza, 2019.

TFG: [Analisis e implementacion de nuevas funcionalidades en un entorno WLAN Enterprise basado en SDN](https://zaguan.unizar.es/record/69869) / Ruben Munilla Hernandez / University of Zaragoza, 2017.

TFG: [Analisis de la eficiencia en tuneles LISP con multiplexion y seguridad](https://zaguan.unizar.es/record/64271?ln=es) / Andrés Garcia Cavero, University of Zaragoza, 2017.

TFG: [Agrupamiento de tramas en Wi-Fi: Mejora de la eficiencia en la red a través del mecanismo de agregación](https://zaguan.unizar.es/record/61110?ln=es) / Cristian Hernandez, University of Zaragoza, 2016.

TFG: [Optimizacion de Trafico de Red mediante Compresion, Multiplexion y Tunelado: Analisis y Caracterizacion](https://zaguan.unizar.es/record/47459?ln=es) / Ignacio Forcen Cabrejas, University of Zaragoza, 2015.

PFC [Smart City: Sistema para acercar el pequeño comercio al ciudadano (MyCity4Me)](https://zaguan.unizar.es/record/14534?ln=es) / Jesus Martin Gonzalez, University of Zaragoza, 2014.

PFC: [Análisis de prestaciones de un sistema de videoconferencia comercial](https://zaguan.unizar.es/record/12107?ln=es) / Carlos Fernandez Barbera, University of Zaragoza, 2013.

TFM: [Movilidad IP en redes heterogeneas: optimizacion de flujos de tráfico con QoS](https://zaguan.unizar.es/record/12883?ln=es) / Andres Chesa Badia, University of Zaragoza, 2013.

PFC: Seguridad y Alta Disponibilidad en un Sistema de ToIP / Adrian Rejas Conde, University of Zaragoza, 2010.

PFC: Analisis de un sistema CAC para Telefonia IP / Jennifer Murillo Royo, 2010.

PFC: Implementacion de control de admision en sistemas ToIP / Laura Esteban Boillos, University of Zaragoza, 2009.
