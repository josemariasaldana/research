[Home](/README.md)

## Patents

### US Patent 20230034345

Javaudin, Bonnamy, SALDANA, RUIZ MAS, FERNANDEZ NAVAJAS. (2020). Improved switching between physical access points sharing the same virtual access point identifier

https://patents.google.com/patent/US20230034345A1/en
U.S. Patent 20230034345
US Patent App. 17/786,895
Publication date: February 2, 2023


### France patent WO2021123582A1

Javaudin, Bonnamy, SALDANA, RUIZ MAS, FERNANDEZ NAVAJAS. (2020). [Improved switching between physical access points sharing the same virtual access point identifier](https://patentscope.wipo.int/search/en/detail.jsf?docId=WO2021123582).

Improved switching between physical access points sharing the same virtual access point identifier. The invention relates to a method for managing the switching of a terminal (STA) between a plurality of physical access points (AP1, AP2) to a Wi-Fi network, a virtual access point identifier (LVAP) dedicated to the terminal being assigned by a controller (CTL) to a first physical access point (AP1), the terminal associating with the first physical access point by a first connection using the virtual access point identifier, the method comprising at the first access point:  transmitting (1003), from the first physical access point to the controller, information relating to the capabilities of the terminal,  receiving (1010) an order to switch from the first connection to a second connection established between the terminal and a second physical access point (AP2) of the plurality using the same virtual access point identifier, the first and the second connections being adapted to the capabilities of the terminal.

https://patents.google.com/patent/WO2021123582A1/en

Country/Location of publication: France
2020-12-14 Application filed by Orange (Date of register)
2020-12-14 Priority to EP20845189.8A
2020-12-14 Priority to US17/786,895
2020-12-14 Priority to CN202080095125.0A
2021-06-24 Publication of WO2021123582A1 (Conferral date)
Entity holder of rights: ORANGE Société anonyme.
Reference/registry code: PCT/FR2020/052405
Nº of application: WO2021123582
Nº of patent: WO/2021/123582
